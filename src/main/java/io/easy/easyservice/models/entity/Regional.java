package io.easy.easyservice.models.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "regional")
@Data
public class Regional {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer regionalid;
    private String regionaldept;
    private LocalDateTime datecreated;
}
