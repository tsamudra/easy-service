package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface CompanyRepository extends JpaRepository<Company, Integer> {

    Company getByCompanyid(Integer id);
    Company getByCompanyname(String companyname);
    @Transactional
    Integer removeByCompanyid(Integer id);

}
