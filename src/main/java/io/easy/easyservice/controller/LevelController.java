package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.LevelDao;
import io.easy.easyservice.models.entity.Level;
import io.easy.easyservice.services.LevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class LevelController {
    @Autowired
    private LevelService levelService;

    @GetMapping("/getalllevel")
    public List<Level> getAllLevel(){
        return levelService.getAll();
    }

    @GetMapping("/getlevelbyid")
    public Level getLevelById(@RequestParam Integer id){
        return levelService.getById(id);
    }

    @GetMapping("/getlevelbyname")
    public Level getLevelByName(@RequestParam String levelname){
        return levelService.getByName(levelname);
    }

    @PostMapping("/createlevel")
    public Level createLevel(@RequestBody LevelDao levelDao){
        return levelService.creteLevel(levelDao);
    }

    @PatchMapping("/updatelevel")
    public Level updateLevel(@RequestBody LevelDao levelDao){
        return levelService.updateLevel(levelDao);
    }

    @DeleteMapping("/deletelevel")
    public Integer deleteLevel(@RequestParam Integer id){
        return levelService.deleteLevel(id);
    }
}
