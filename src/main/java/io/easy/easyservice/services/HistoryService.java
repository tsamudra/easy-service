package io.easy.easyservice.services;

import io.easy.easyservice.models.entity.History;
import io.easy.easyservice.models.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryService {

    @Autowired
    private HistoryRepository historyRepository;

    public List<History> getAllByCode(String code) {
        return historyRepository.findAllByHistorycode(code);
    }

}
