package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.RoleDao;
import io.easy.easyservice.models.entity.Role;
import io.easy.easyservice.models.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public List<Role> getAllRoles () {
        return roleRepository.findAll();
    }

    public Role getRoleDetailsbyRolename (String rolename) {
        return roleRepository.getByRolename(rolename);
    }

    public Role getRoleDetailsbyId (Integer id) {
        return roleRepository.getByRoleid(id);
    }

    public Role createNewRole(RoleDao role){
        Role checkAvailability = getRoleDetailsbyRolename(role.getRolename());
        if (Objects.isNull(checkAvailability)) {
            LocalDateTime now = LocalDateTime.now();
            Role temprole = new Role();
            temprole.setRoleid(null);
            temprole.setRolename(role.getRolename());
            temprole.setDatecreated(now);
            return roleRepository.saveAndFlush(temprole);
        } else {
            return new Role();
        }
    }

    public Role updateRole(RoleDao roleDao){
        Role roleFromDB = getRoleDetailsbyRolename(roleDao.getRolename());
        if (!Objects.isNull(roleFromDB)) {
            roleFromDB.setRolename(roleDao.getRolename());
            return roleRepository.saveAndFlush(roleFromDB);
        } else {
            return new Role();
        }
    }

    public Integer deleteRole(Integer id){
        return roleRepository.removeByRoleid(id);
    }
}
