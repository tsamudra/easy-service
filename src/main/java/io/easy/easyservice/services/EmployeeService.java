package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.BulkDao;
import io.easy.easyservice.models.dao.EmployeeDao;
import io.easy.easyservice.models.dao.EmployeeReportDao;
import io.easy.easyservice.models.entity.*;
import io.easy.easyservice.models.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EducationRepository educationRepository;

    @Autowired
    private RegionalRepository regionalRepository;

    @Autowired
    private DivisionRepository divisionRepository;

    @Autowired
    private MainjobRepository mainjobRepository;

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private HistoryRepository historyRepository;

    public List<Employee> getAllActive(){
        Status status = statusRepository.findByStatus("ACTIVE");
        return employeeRepository.findAllByStatus(status);
    }

    public List<Employee> getAllActiveWithFilter(String company, String regional, String division, String position){
        List<Company> companies = this.getAllCompanyName(company);
        List<Regional> regionals = this.getAllRegionalDept(regional);
        List<Division> divisions = this.getAllDivisionName(division);
        List<Position> positions = this.getAllPositionName(position);
        Status status = statusRepository.findByStatus("ACTIVE");
        return employeeRepository.findAllByStatusAndCompanyInAndRegionalInAndDivisionInAndPositionIn(status,companies,regionals,divisions,positions);
    }

    public List<Employee> getAllResignWithFilter(String startdate, String enddate, String company){
        List<Company> listCompanies = this.getAllCompanyName(company);
        List<Integer> companies = new ArrayList<>();
        for (int i = 0; i < listCompanies.size(); i++){
            companies.add(listCompanies.get(i).getCompanyid());
        }
        String start = "";
        String end = "";
        if (startdate.equals("")){
            start = "01-01-1999";
        } else {
            start = startdate;
        }

        if (enddate.equals("")){
            end = this.getDDMMYYYY();
        } else {
            end = enddate;
        }
        System.out.println(start);
        System.out.println(end);
        return employeeRepository.customResignEmployeeFilters(start, end, companies);
    }

    public List<Employee> getAllOffWithFilter(String startdate, String enddate){
        String start = "";
        String end = "";
        if (startdate.equals("")){
            start = "01-01-1999";
        } else {
            start = startdate;
        }

        if (enddate.equals("")){
            end = this.getDDMMYYYY();
        } else {
            end = enddate;
        }
        return employeeRepository.customOffEmployeeFilters(start, end);
    }

    public List<Employee> getAllOff(){
        Status status = statusRepository.findByStatus("OFF");
        return employeeRepository.findAllByStatus(status);
    }

    public List<Employee> getAllResign(){
        Status status = statusRepository.findByStatus("RESIGN");
        return employeeRepository.findAllByStatus(status);
    }

    public Employee getByid(Integer id){
        return employeeRepository.getByEmployeeid(id);
    }

    public Employee getEmpActiveById(Integer id){
        Status status = statusRepository.findByStatus("ACTIVE");
        return employeeRepository.getByEmployeeidAndStatus(id, status);
    }

    public Employee getByCallsign(String callsign){
        Status status = statusRepository.findByStatus("ACTIVE");
        return employeeRepository.getByCallsignAndStatus(callsign, status);
    }

    public Employee getByNikKtp(String nik){
        return employeeRepository.getByNikktp(nik);
    }

    public Employee getByNIK(String nik){
        Status status = statusRepository.findByStatus("ACTIVE");
        return employeeRepository.getByNikAndStatus(nik, status);
    }

    public Employee getByHistoryCode(String code){
        return employeeRepository.getByHistorycode(code);
    }

    public String deleteEmployee(BulkDao bulkDao){
        List<Integer> ids = bulkDao.getIds();
        String username = bulkDao.getUsername();
        Integer totalData = ids.size();
        Integer counter = 0;
        Integer error = 0;
        for(int i = 0; i < ids.size(); i++){
            Integer res = employeeRepository.removeByEmployeeid(ids.get(i));
            if (res.equals(1)){
                counter++;
            } else {
                error++;
            }
        }

        if (totalData.equals(counter)){
            return counter + " of " + totalData + " Deleted";
        } else {
            return error + " Data(s) Failed to Delete";
        }
    }

    public String restoreEmployee(BulkDao bulkDao){
        List<Integer> ids = bulkDao.getIds();
        String username = bulkDao.getUsername();
        Integer totalData = ids.size();
        Integer counter = 0;
        Integer error = 0;
        for(int i = 0; i < ids.size(); i++){
            Status status = statusRepository.findByStatus("ACTIVE");
            Employee employee = this.getByid(ids.get(i));
            employee.setStatus(status);
            employee.setModifiedby(username);
            employee.setModifiedon(LocalDateTime.now());
            Employee response = employeeRepository.saveAndFlush(employee);
            if (!Objects.isNull(response.getEmployeeid())){
                counter++;
            } else {
                error++;
            }
        }

        if (totalData.equals(counter)){
            return counter + " of " + totalData + " Restored";
        } else {
            return error + " Data(s) Failed to Restore";
        }
    }

    public Employee createEmployee(EmployeeDao employeeDao){
        String historyCode = this.fixedHistoryCode();
        Employee checkCallsign = getByCallsign(employeeDao.getCallsign());
        Employee checkNIKKTP = getByNikKtp(employeeDao.getNikktp());
        Employee checkNIK = getByNIK(employeeDao.getNik());
        if (Objects.isNull(checkCallsign) && Objects.isNull(checkNIKKTP) && Objects.isNull(checkNIK)){
            LocalDateTime now = LocalDateTime.now();
            Employee newEmployee = new Employee();
            Education education = educationRepository.findByEduid(employeeDao.getEducationid());
            Regional regional = regionalRepository.getByRegionalid(employeeDao.getRegionalid());
            Division division = divisionRepository.findByDivisionid(employeeDao.getDivisionid());
            MainJob mainJob = mainjobRepository.getByMainjobid(employeeDao.getMainjobid());
            Position position = positionRepository.getByPositionid(employeeDao.getPositionid());
            Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
            Company company = companyRepository.getByCompanyid(employeeDao.getCompanyid());
            Status status = statusRepository.getById(2);
            newEmployee.setEmployeeid(null);
            newEmployee.setFullname(employeeDao.getFullname());
            newEmployee.setCallsign(employeeDao.getCallsign());
            newEmployee.setAddress(employeeDao.getAddress());
            newEmployee.setNik(employeeDao.getNik());
            newEmployee.setNikktp(employeeDao.getNikktp());
            newEmployee.setEducation(education);
            newEmployee.setPhonenum(employeeDao.getPhonenum());
            newEmployee.setRegional(regional);
            newEmployee.setDivision(division);
            newEmployee.setMainJob(mainJob);
            newEmployee.setPosition(position);
            newEmployee.setLevel(level);
            newEmployee.setJoindate(employeeDao.getJoindate());
            newEmployee.setJoincallsign(employeeDao.getJoincallsign());
            newEmployee.setReleasecallsign(null);
            newEmployee.setCompany(company);
            newEmployee.setEmail(employeeDao.getEmail());
            newEmployee.setRemark(employeeDao.getRemark());
            newEmployee.setRemark_resign(null);
            newEmployee.setResigndate(null);
            newEmployee.setStatus(status);
            newEmployee.setDatecreated(now);
            newEmployee.setCreatedby(employeeDao.getCreatedby());
            newEmployee.setHistorycode(historyCode);

            //history add
            History newHistory = new History();
            newHistory.setType("NEW");
            newHistory.setCreateddate(now);
            newHistory.setJoincallsign(employeeDao.getJoincallsign());
            newHistory.setCreatedby(employeeDao.getCreatedby());
            newHistory.setHistorycode(historyCode);
            historyRepository.saveAndFlush(newHistory);
            //end history add

            return employeeRepository.saveAndFlush(newEmployee);
        } else {
            if(!Objects.isNull(checkCallsign)) {
                Employee employee = new Employee();
                employee.setFullname("Callsign Used, Please Use Another Callsign / Create New Callsign");
                return employee;
            } else if (!Objects.isNull(checkNIKKTP)){
                Employee employee = new Employee();
                employee.setFullname("NIK KTP Ever Registered in Database, Cannot Save Duplicate NIK KTP");
                return employee;
            } else if (!Objects.isNull(checkNIK)) {
                Employee employee = new Employee();
                employee.setFullname("NIK Registered, Cannot Save Duplicate NIK");
                return employee;
            } else {
                Employee employee = new Employee();
                employee.setFullname("Duplicate Data, Please Check Callsign / NIK KTP / NIK");
                return employee;
            }
        }
    }

    public Employee updateEmployeeDetails(EmployeeDao employeeDao){
        Employee employeeFromDB = getEmpActiveById(employeeDao.getEmployeeid());
        Employee checkNIKKTP = getByNikKtp(employeeDao.getNikktp());
        Employee checkNIK =getByNIK(employeeDao.getNik());
        if(!Objects.isNull(employeeFromDB)) {
            if(employeeFromDB.getNikktp().equals(employeeDao.getNikktp())) {
                if (employeeFromDB.getNik().equals(employeeDao.getNik())) {
                    LocalDateTime now = LocalDateTime.now();
                    Education education = educationRepository.findByEduid(employeeDao.getEducationid());
                    Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
                    employeeFromDB.setNikktp(employeeDao.getNikktp());
                    employeeFromDB.setNik(employeeDao.getNik());
                    employeeFromDB.setFullname(employeeDao.getFullname());
                    employeeFromDB.setAddress(employeeDao.getAddress());
                    employeeFromDB.setEducation(education);
                    employeeFromDB.setPhonenum(employeeDao.getPhonenum());
                    employeeFromDB.setLevel(level);
                    employeeFromDB.setEmail(employeeDao.getEmail());
                    employeeFromDB.setRemark(employeeDao.getRemark());
                    employeeFromDB.setModifiedby(employeeDao.getCreatedby());
                    employeeFromDB.setModifiedon(now);
                    return employeeRepository.saveAndFlush(employeeFromDB);
                } else {
                    if (Objects.isNull(checkNIK)){
                        LocalDateTime now = LocalDateTime.now();
                        Education education = educationRepository.findByEduid(employeeDao.getEducationid());
                        Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
                        employeeFromDB.setNikktp(employeeDao.getNikktp());
                        employeeFromDB.setNik(employeeDao.getNik());
                        employeeFromDB.setFullname(employeeDao.getFullname());
                        employeeFromDB.setAddress(employeeDao.getAddress());
                        employeeFromDB.setEducation(education);
                        employeeFromDB.setPhonenum(employeeDao.getPhonenum());
                        employeeFromDB.setLevel(level);
                        employeeFromDB.setEmail(employeeDao.getEmail());
                        employeeFromDB.setRemark(employeeDao.getRemark());
                        employeeFromDB.setModifiedby(employeeDao.getCreatedby());
                        employeeFromDB.setModifiedon(now);
                        return employeeRepository.saveAndFlush(employeeFromDB);
                    } else {
                        Employee employee = new Employee();
                        employee.setFullname("NIK Registered, Cannot Save Duplicate NIK");
                        return employee;
                    }
                }
            } else {
                if(Objects.isNull(checkNIKKTP)) {
                    Education education = educationRepository.findByEduid(employeeDao.getEducationid());
                    Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
                    employeeFromDB.setNikktp(employeeDao.getNikktp());
                    employeeFromDB.setNik(employeeDao.getNik());
                    employeeFromDB.setFullname(employeeDao.getFullname());
                    employeeFromDB.setAddress(employeeDao.getAddress());
                    employeeFromDB.setEducation(education);
                    employeeFromDB.setPhonenum(employeeDao.getPhonenum());
                    employeeFromDB.setLevel(level);
                    employeeFromDB.setEmail(employeeDao.getEmail());
                    employeeFromDB.setRemark(employeeDao.getRemark());
                    return employeeRepository.saveAndFlush(employeeFromDB);
                } else {
                    Employee employee = new Employee();
                    employee.setFullname("NIK KTP Registered, Cannot Save Duplicate NIK KTP");
                    return employee;
                }
            }
        } else {
            Employee employee = new Employee();
            return employee;
        }
    }

    public Employee updateCallsign(EmployeeDao employeeDao){
        Employee checkCallsign = getByCallsign(employeeDao.getCallsign());
        Employee employee = getEmpActiveById(employeeDao.getEmployeeid());
        Employee newEmployee = new Employee();
        if (Objects.isNull(checkCallsign)){
            LocalDateTime now = LocalDateTime.now();
            Company company = companyRepository.getByCompanyid(employee.getCompany().getCompanyid());
            Division division = divisionRepository.findByDivisionid(employee.getDivision().getDivisionid());
            Education education = educationRepository.findByEduid(employee.getEducation().getEduid());
            MainJob mainJob = mainjobRepository.getByMainjobid(employee.getMainJob().getMainjobid());
            Position position = positionRepository.getByPositionid(employeeDao.getPositionid());
            Regional regional = regionalRepository.getByRegionalid(employee.getRegional().getRegionalid());
            Status status = statusRepository.findByStatus(employee.getStatus().getStatus());
            Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
            newEmployee.setAddress(employee.getAddress());
            newEmployee.setCallsign(employeeDao.getCallsign());
            newEmployee.setFullname(employeeDao.getFullname());
            newEmployee.setLevel(level);
            newEmployee.setJoindate(employee.getJoindate());
            newEmployee.setNik(employee.getNik());
            newEmployee.setNikktp(employee.getNikktp());
            newEmployee.setPhonenum(employee.getPhonenum());
            newEmployee.setResigndate(null);
            newEmployee.setDatecreated(now);
            newEmployee.setEmail(employee.getEmail());
            newEmployee.setJoincallsign(employee.getJoincallsign());
            newEmployee.setReleasecallsign(null);
            newEmployee.setRemark(employee.getRemark());
            newEmployee.setRemark_movement(employee.getRemark_movement());
            newEmployee.setRemark_resign(employee.getRemark_resign());
            newEmployee.setRemark_promote(employee.getRemark_promote());
            newEmployee.setCreatedby(employeeDao.getCreatedby());
            newEmployee.setCompany(company);
            newEmployee.setDivision(division);
            newEmployee.setEducation(education);
            newEmployee.setMainJob(mainJob);
            newEmployee.setPosition(position);
            newEmployee.setRegional(regional);
            newEmployee.setStatus(status);
            newEmployee.setHistorycode(employee.getHistorycode());
            newEmployee.setModifiedon(now);
            newEmployee.setModifiedby(employeeDao.getCreatedby());
            //inactive old employee data
            Status newStatus = statusRepository.findByStatus("INACTIVE");
            employee.setStatus(newStatus);
            employee.setReleasecallsign(this.getDDMMYYYY());
            employee.setModifiedby(employeeDao.getCreatedby());
            employee.setModifiedon(now);
            employeeRepository.saveAndFlush(employee);
            //end inactive old employee data

            Employee response = employeeRepository.saveAndFlush(newEmployee);

            //history update callsign
            History newHistory = new History();
            newHistory.setType("UPDATE");
            newHistory.setCreateddate(now);
            newHistory.setHistoryfromto("CALLSIGN");
            newHistory.setEmployeeidfrom(employee.getEmployeeid());
            newHistory.setEmployeeidto(response.getEmployeeid());
            newHistory.setCallsignfrom(employee.getCallsign());
            newHistory.setCallsignto(employeeDao.getCallsign());
            if(employee.getLevel().getLevelid().equals(employeeDao.getLevelid())){
                newHistory.setLevelfrom(null);
                newHistory.setLevelto(null);
            } else {
                newHistory.setLevelfrom(employee.getLevel().getLevelname());
                newHistory.setLevelto(response.getLevel().getLevelname());
            }
            newHistory.setJoincallsign(this.getDDMMYYYY());
            newHistory.setCreatedby(employeeDao.getCreatedby());
            newHistory.setHistorycode(employee.getHistorycode());
            historyRepository.saveAndFlush(newHistory);
            //end history update callsign


            return response;
        } else {
            Employee response = new Employee();
            response.setFullname("Callsign Used, Please Choose Another Callsign / Create New Callsign");
            return response;
        }
    }

    public Employee updatePromote(EmployeeDao employeeDao){
        Employee checkCallsign = getByCallsign(employeeDao.getCallsign());
        Employee employee = getEmpActiveById(employeeDao.getEmployeeid());
        Employee newEmployee = new Employee();
        if(employee.getCallsign().equals(employeeDao.getCallsign())){
            LocalDateTime now = LocalDateTime.now();
            Education education = educationRepository.findByEduid(employee.getEducation().getEduid());
            Company company = companyRepository.getByCompanyid(employee.getCompany().getCompanyid());
            Division division = divisionRepository.findByDivisionid(employeeDao.getDivisionid());
            MainJob mainJob = mainjobRepository.getByMainjobid(employeeDao.getMainjobid());
            Position position = positionRepository.getByPositionid(employeeDao.getPositionid());
            Regional regional = regionalRepository.getByRegionalid(employeeDao.getRegionalid());
            Status status = statusRepository.findByStatus(employee.getStatus().getStatus());
            Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
            newEmployee.setAddress(employee.getAddress());
            newEmployee.setCallsign(employeeDao.getCallsign());
            newEmployee.setFullname(employeeDao.getFullname());
            newEmployee.setLevel(level);
            newEmployee.setJoindate(employee.getJoindate());
            newEmployee.setNik(employee.getNik());
            newEmployee.setNikktp(employee.getNikktp());
            newEmployee.setPhonenum(employee.getPhonenum());
            newEmployee.setResigndate(null);
            newEmployee.setDatecreated(now);
            newEmployee.setEmail(employee.getEmail());
            newEmployee.setJoincallsign(employee.getJoincallsign());
            newEmployee.setReleasecallsign(null);
            newEmployee.setRemark(employee.getRemark());
            newEmployee.setRemark_movement(employee.getRemark_movement());
            newEmployee.setRemark_resign(employee.getRemark_resign());
            newEmployee.setRemark_promote(employee.getRemark_promote());
            newEmployee.setCreatedby(employeeDao.getCreatedby());
            newEmployee.setCompany(company);
            newEmployee.setDivision(division);
            newEmployee.setEducation(education);
            newEmployee.setMainJob(mainJob);
            newEmployee.setPosition(position);
            newEmployee.setRegional(regional);
            newEmployee.setStatus(status);
            newEmployee.setHistorycode(employee.getHistorycode());
            newEmployee.setModifiedon(now);
            newEmployee.setModifiedby(employeeDao.getCreatedby());
            //inactive old employee data
            Status newStatus = statusRepository.findByStatus("INACTIVE");
            employee.setStatus(newStatus);
            employee.setModifiedon(now);
            employee.setModifiedby(employeeDao.getCreatedby());
            employeeRepository.saveAndFlush(employee);
            //end inactive old employee data

            Employee response = employeeRepository.saveAndFlush(newEmployee);

            //history update promote
            History newHistory = new History();
            newHistory.setType("UPDATE");
            newHistory.setCreateddate(now);
            newHistory.setHistoryfromto("PROMOTE");
            newHistory.setEmployeeidfrom(employee.getEmployeeid());
            newHistory.setEmployeeidto(response.getEmployeeid());
            if (employee.getCallsign().equals(employeeDao.getCallsign())){
                newHistory.setCallsignfrom(null);
                newHistory.setCallsignto(null);
            } else {
                newHistory.setCallsignfrom(employee.getCallsign());
                newHistory.setCallsignto(employeeDao.getCallsign());
            }
            if(employee.getLevel().getLevelid().equals(employeeDao.getLevelid())){
                newHistory.setLevelfrom(null);
                newHistory.setLevelto(null);
            } else {
                newHistory.setLevelfrom(employee.getLevel().getLevelname());
                newHistory.setLevelto(response.getLevel().getLevelname());
            }
            if(employee.getRegional().getRegionalid().equals(employeeDao.getRegionalid())){
                newHistory.setRegionalfrom(null);
                newHistory.setRegionalto(null);
            } else {
                newHistory.setRegionalfrom(employee.getRegional().getRegionaldept());
                newHistory.setRegionalto(response.getRegional().getRegionaldept());
            }
            if(employee.getDivision().getDivisionid().equals(employeeDao.getDivisionid())){
                newHistory.setDivisionfrom(null);
                newHistory.setDivisionto(null);
            } else {
                newHistory.setDivisionfrom(employee.getDivision().getDivisioname());
                newHistory.setDivisionto(response.getDivision().getDivisioname());
            }
            if(employee.getMainJob().getMainjobid().equals(employeeDao.getMainjobid())){
                newHistory.setMainjobfrom(null);
                newHistory.setMainjobto(null);
            } else {
                newHistory.setMainjobfrom(employee.getMainJob().getMainjobname());
                newHistory.setMainjobto(response.getMainJob().getMainjobname());
            }
            if(employee.getPosition().getPositionid().equals(employeeDao.getPositionid())){
                newHistory.setPositionfrom(null);
                newHistory.setPositionto(null);
            } else {
                newHistory.setPositionfrom(employee.getPosition().getPositionname());
                newHistory.setPositionto(response.getPosition().getPositionname());
            }
            newHistory.setJoincallsign(employee.getJoincallsign());
            newHistory.setCreatedby(employeeDao.getCreatedby());
            newHistory.setHistorycode(employee.getHistorycode());
            historyRepository.saveAndFlush(newHistory);
            //end history update promote


            return response;
        } else {
            if (Objects.isNull(checkCallsign)){
                LocalDateTime now = LocalDateTime.now();
                Education education = educationRepository.findByEduid(employee.getEducation().getEduid());
                Company company = companyRepository.getByCompanyid(employee.getCompany().getCompanyid());
                Division division = divisionRepository.findByDivisionid(employeeDao.getDivisionid());
                MainJob mainJob = mainjobRepository.getByMainjobid(employeeDao.getMainjobid());
                Position position = positionRepository.getByPositionid(employeeDao.getPositionid());
                Regional regional = regionalRepository.getByRegionalid(employeeDao.getRegionalid());
                Status status = statusRepository.findByStatus(employee.getStatus().getStatus());
                Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
                newEmployee.setAddress(employee.getAddress());
                newEmployee.setCallsign(employeeDao.getCallsign());
                newEmployee.setFullname(employeeDao.getFullname());
                newEmployee.setLevel(level);
                newEmployee.setJoindate(employee.getJoindate());
                newEmployee.setNik(employee.getNik());
                newEmployee.setNikktp(employee.getNikktp());
                newEmployee.setPhonenum(employee.getPhonenum());
                newEmployee.setResigndate(null);
                newEmployee.setDatecreated(now);
                newEmployee.setEmail(employee.getEmail());
                newEmployee.setJoincallsign(this.getDDMMYYYY());
                newEmployee.setReleasecallsign(null);
                newEmployee.setRemark(employee.getRemark());
                newEmployee.setRemark_movement(employee.getRemark_movement());
                newEmployee.setRemark_resign(employee.getRemark_resign());
                newEmployee.setRemark_promote(employee.getRemark_promote());
                newEmployee.setCreatedby(employeeDao.getCreatedby());
                newEmployee.setCompany(company);
                newEmployee.setDivision(division);
                newEmployee.setEducation(education);
                newEmployee.setMainJob(mainJob);
                newEmployee.setPosition(position);
                newEmployee.setRegional(regional);
                newEmployee.setStatus(status);
                newEmployee.setHistorycode(employee.getHistorycode());
                newEmployee.setModifiedon(now);
                newEmployee.setModifiedby(employeeDao.getCreatedby());
                //inactive old employee data
                Status newStatus = statusRepository.findByStatus("INACTIVE");
                employee.setStatus(newStatus);
                employee.setReleasecallsign(this.getDDMMYYYY());
                employee.setModifiedon(now);
                employee.setModifiedby(employeeDao.getCreatedby());
                employeeRepository.saveAndFlush(employee);
                //end inactive old employee data

                Employee response = employeeRepository.saveAndFlush(newEmployee);

                //history update promote
                History newHistory = new History();
                newHistory.setType("UPDATE");
                newHistory.setCreateddate(now);
                newHistory.setHistoryfromto("PROMOTE");
                newHistory.setEmployeeidfrom(employee.getEmployeeid());
                newHistory.setEmployeeidto(response.getEmployeeid());
                if (employee.getCallsign().equals(employeeDao.getCallsign())){
                    newHistory.setCallsignfrom(null);
                    newHistory.setCallsignto(null);
                } else {
                    newHistory.setCallsignfrom(employee.getCallsign());
                    newHistory.setCallsignto(employeeDao.getCallsign());
                }
                if(employee.getLevel().getLevelid().equals(employeeDao.getLevelid())){
                    newHistory.setLevelfrom(null);
                    newHistory.setLevelto(null);
                } else {
                    newHistory.setLevelfrom(employee.getLevel().getLevelname());
                    newHistory.setLevelto(response.getLevel().getLevelname());
                }
                if(employee.getRegional().getRegionalid().equals(employeeDao.getRegionalid())){
                    newHistory.setRegionalfrom(null);
                    newHistory.setRegionalto(null);
                } else {
                    newHistory.setRegionalfrom(employee.getRegional().getRegionaldept());
                    newHistory.setRegionalto(response.getRegional().getRegionaldept());
                }
                if(employee.getDivision().getDivisionid().equals(employeeDao.getDivisionid())){
                    newHistory.setDivisionfrom(null);
                    newHistory.setDivisionto(null);
                } else {
                    newHistory.setDivisionfrom(employee.getDivision().getDivisioname());
                    newHistory.setDivisionto(response.getDivision().getDivisioname());
                }
                if(employee.getMainJob().getMainjobid().equals(employeeDao.getMainjobid())){
                    newHistory.setMainjobfrom(null);
                    newHistory.setMainjobto(null);
                } else {
                    newHistory.setMainjobfrom(employee.getMainJob().getMainjobname());
                    newHistory.setMainjobto(response.getMainJob().getMainjobname());
                }
                if(employee.getPosition().getPositionid().equals(employeeDao.getPositionid())){
                    newHistory.setPositionfrom(null);
                    newHistory.setPositionto(null);
                } else {
                    newHistory.setPositionfrom(employee.getPosition().getPositionname());
                    newHistory.setPositionto(response.getPosition().getPositionname());
                }
                newHistory.setJoincallsign(this.getDDMMYYYY());
                newHistory.setCreatedby(employeeDao.getCreatedby());
                newHistory.setHistorycode(employee.getHistorycode());
                historyRepository.saveAndFlush(newHistory);
                //end history update promote


                return response;
            } else {
                Employee response = new Employee();
                response.setFullname("Callsign Used, Please Choose Another Callsign / Create New Callsign");
                return response;
            }
        }

    }

    public Employee updateMovement(EmployeeDao employeeDao){
        Employee checkCallsign = getByCallsign(employeeDao.getCallsign());
        Employee checkNIK = getByNIK(employeeDao.getNik());
        Employee employee = getEmpActiveById(employeeDao.getEmployeeid());
        Employee newEmployee = new Employee();
        if(employee.getCallsign().equals(employeeDao.getCallsign())){
            LocalDateTime now = LocalDateTime.now();
            Education education = educationRepository.findByEduid(employee.getEducation().getEduid());
            Company company = companyRepository.getByCompanyid(employeeDao.getCompanyid());
            Division division = divisionRepository.findByDivisionid(employeeDao.getDivisionid());
            MainJob mainJob = mainjobRepository.getByMainjobid(employeeDao.getMainjobid());
            Position position = positionRepository.getByPositionid(employeeDao.getPositionid());
            Regional regional = regionalRepository.getByRegionalid(employeeDao.getRegionalid());
            Status status = statusRepository.findByStatus(employee.getStatus().getStatus());
            Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
            newEmployee.setAddress(employee.getAddress());
            newEmployee.setCallsign(employeeDao.getCallsign());
            newEmployee.setFullname(employeeDao.getFullname());
            newEmployee.setLevel(level);
            newEmployee.setJoindate(employee.getJoindate());
            if(employee.getNik().equals(employeeDao.getNik())){
                newEmployee.setNik(employee.getNik());
            } else {
                if (Objects.isNull(checkNIK)){
                    newEmployee.setNik(employeeDao.getNik());
                } else {
                    Employee response = new Employee();
                    response.setFullname("NIK Used, Please Choose Another NIK");
                    return response;
                }
            }
            newEmployee.setNikktp(employee.getNikktp());
            newEmployee.setPhonenum(employee.getPhonenum());
            newEmployee.setResigndate(null);
            newEmployee.setDatecreated(now);
            newEmployee.setEmail(employee.getEmail());
            newEmployee.setJoincallsign(employee.getJoincallsign());
            newEmployee.setReleasecallsign(null);
            newEmployee.setRemark(employee.getRemark());
            newEmployee.setRemark_movement(employee.getRemark_movement());
            newEmployee.setRemark_resign(employee.getRemark_resign());
            newEmployee.setRemark_promote(employee.getRemark_promote());
            newEmployee.setCreatedby(employeeDao.getCreatedby());
            newEmployee.setCompany(company);
            newEmployee.setDivision(division);
            newEmployee.setEducation(education);
            newEmployee.setMainJob(mainJob);
            newEmployee.setPosition(position);
            newEmployee.setRegional(regional);
            newEmployee.setStatus(status);
            newEmployee.setHistorycode(employee.getHistorycode());
            newEmployee.setModifiedon(now);
            newEmployee.setModifiedby(employeeDao.getCreatedby());
            //inactive old employee data
            Status newStatus = statusRepository.findByStatus("INACTIVE");
            employee.setStatus(newStatus);
            employee.setModifiedon(now);
            employee.setModifiedby(employeeDao.getCreatedby());
            employeeRepository.saveAndFlush(employee);
            //end inactive old employee data

            Employee response = employeeRepository.saveAndFlush(newEmployee);

            //history update promote
            History newHistory = new History();
            newHistory.setType("UPDATE");
            newHistory.setCreateddate(now);
            newHistory.setHistoryfromto("MOVEMENT");
            newHistory.setEmployeeidfrom(employee.getEmployeeid());
            newHistory.setEmployeeidto(response.getEmployeeid());
            if (employee.getCallsign().equals(employeeDao.getCallsign())){
                newHistory.setCallsignfrom(null);
                newHistory.setCallsignto(null);
            } else {
                newHistory.setCallsignfrom(employee.getCallsign());
                newHistory.setCallsignto(employeeDao.getCallsign());
            }
            if(employee.getLevel().getLevelid().equals(employeeDao.getLevelid())){
                newHistory.setLevelfrom(null);
                newHistory.setLevelto(null);
            } else {
                newHistory.setLevelfrom(employee.getLevel().getLevelname());
                newHistory.setLevelto(response.getLevel().getLevelname());
            }
            if(employee.getRegional().getRegionalid().equals(employeeDao.getRegionalid())){
                newHistory.setRegionalfrom(null);
                newHistory.setRegionalto(null);
            } else {
                newHistory.setRegionalfrom(employee.getRegional().getRegionaldept());
                newHistory.setRegionalto(response.getRegional().getRegionaldept());
            }
            if(employee.getDivision().getDivisionid().equals(employeeDao.getDivisionid())){
                newHistory.setDivisionfrom(null);
                newHistory.setDivisionto(null);
            } else {
                newHistory.setDivisionfrom(employee.getDivision().getDivisioname());
                newHistory.setDivisionto(response.getDivision().getDivisioname());
            }
            if(employee.getMainJob().getMainjobid().equals(employeeDao.getMainjobid())){
                newHistory.setMainjobfrom(null);
                newHistory.setMainjobto(null);
            } else {
                newHistory.setMainjobfrom(employee.getMainJob().getMainjobname());
                newHistory.setMainjobto(response.getMainJob().getMainjobname());
            }
            if(employee.getPosition().getPositionid().equals(employeeDao.getPositionid())){
                newHistory.setPositionfrom(null);
                newHistory.setPositionto(null);
            } else {
                newHistory.setPositionfrom(employee.getPosition().getPositionname());
                newHistory.setPositionto(response.getPosition().getPositionname());
            }
            newHistory.setJoincallsign(employee.getJoincallsign());
            newHistory.setCreatedby(employeeDao.getCreatedby());
            newHistory.setHistorycode(employee.getHistorycode());
            historyRepository.saveAndFlush(newHistory);
            //end history update promote


            return response;
        } else {
            if (Objects.isNull(checkCallsign)){
                if (Objects.isNull(checkNIK)) {
                    LocalDateTime now = LocalDateTime.now();
                    Education education = educationRepository.findByEduid(employee.getEducation().getEduid());
                    Company company = companyRepository.getByCompanyid(employeeDao.getCompanyid());
                    Division division = divisionRepository.findByDivisionid(employeeDao.getDivisionid());
                    MainJob mainJob = mainjobRepository.getByMainjobid(employeeDao.getMainjobid());
                    Position position = positionRepository.getByPositionid(employeeDao.getPositionid());
                    Regional regional = regionalRepository.getByRegionalid(employeeDao.getRegionalid());
                    Status status = statusRepository.findByStatus(employee.getStatus().getStatus());
                    Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
                    newEmployee.setAddress(employee.getAddress());
                    newEmployee.setCallsign(employeeDao.getCallsign());
                    newEmployee.setFullname(employeeDao.getFullname());
                    newEmployee.setLevel(level);
                    newEmployee.setJoindate(employee.getJoindate());
                    newEmployee.setNik(employeeDao.getNik());
                    newEmployee.setNikktp(employee.getNikktp());
                    newEmployee.setPhonenum(employee.getPhonenum());
                    newEmployee.setResigndate(null);
                    newEmployee.setDatecreated(now);
                    newEmployee.setEmail(employee.getEmail());
                    newEmployee.setJoincallsign(this.getDDMMYYYY());
                    newEmployee.setReleasecallsign(null);
                    newEmployee.setRemark(employee.getRemark());
                    newEmployee.setRemark_movement(employee.getRemark_movement());
                    newEmployee.setRemark_resign(employee.getRemark_resign());
                    newEmployee.setRemark_promote(employee.getRemark_promote());
                    newEmployee.setCreatedby(employeeDao.getCreatedby());
                    newEmployee.setCompany(company);
                    newEmployee.setDivision(division);
                    newEmployee.setEducation(education);
                    newEmployee.setMainJob(mainJob);
                    newEmployee.setPosition(position);
                    newEmployee.setRegional(regional);
                    newEmployee.setStatus(status);
                    newEmployee.setHistorycode(employee.getHistorycode());
                    newEmployee.setModifiedon(now);
                    newEmployee.setModifiedby(employeeDao.getCreatedby());
                    //inactive old employee data
                    Status newStatus = statusRepository.findByStatus("INACTIVE");
                    employee.setStatus(newStatus);
                    employee.setReleasecallsign(this.getDDMMYYYY());
                    employee.setModifiedon(now);
                    employee.setModifiedby(employeeDao.getCreatedby());
                    employeeRepository.saveAndFlush(employee);
                    //end inactive old employee data

                    Employee response = employeeRepository.saveAndFlush(newEmployee);

                    //history update promote
                    History newHistory = new History();
                    newHistory.setType("UPDATE");
                    newHistory.setCreateddate(now);
                    newHistory.setHistoryfromto("MOVEMENT");
                    newHistory.setEmployeeidfrom(employee.getEmployeeid());
                    newHistory.setEmployeeidto(response.getEmployeeid());
                    if (employee.getCallsign().equals(employeeDao.getCallsign())){
                        newHistory.setCallsignfrom(null);
                        newHistory.setCallsignto(null);
                    } else {
                        newHistory.setCallsignfrom(employee.getCallsign());
                        newHistory.setCallsignto(employeeDao.getCallsign());
                    }
                    if(employee.getLevel().getLevelid().equals(employeeDao.getLevelid())){
                        newHistory.setLevelfrom(null);
                        newHistory.setLevelto(null);
                    } else {
                        newHistory.setLevelfrom(employee.getLevel().getLevelname());
                        newHistory.setLevelto(response.getLevel().getLevelname());
                    }
                    if(employee.getRegional().getRegionalid().equals(employeeDao.getRegionalid())){
                        newHistory.setRegionalfrom(null);
                        newHistory.setRegionalto(null);
                    } else {
                        newHistory.setRegionalfrom(employee.getRegional().getRegionaldept());
                        newHistory.setRegionalto(response.getRegional().getRegionaldept());
                    }
                    if(employee.getDivision().getDivisionid().equals(employeeDao.getDivisionid())){
                        newHistory.setDivisionfrom(null);
                        newHistory.setDivisionto(null);
                    } else {
                        newHistory.setDivisionfrom(employee.getDivision().getDivisioname());
                        newHistory.setDivisionto(response.getDivision().getDivisioname());
                    }
                    if(employee.getMainJob().getMainjobid().equals(employeeDao.getMainjobid())){
                        newHistory.setMainjobfrom(null);
                        newHistory.setMainjobto(null);
                    } else {
                        newHistory.setMainjobfrom(employee.getMainJob().getMainjobname());
                        newHistory.setMainjobto(response.getMainJob().getMainjobname());
                    }
                    if(employee.getPosition().getPositionid().equals(employeeDao.getPositionid())){
                        newHistory.setPositionfrom(null);
                        newHistory.setPositionto(null);
                    } else {
                        newHistory.setPositionfrom(employee.getPosition().getPositionname());
                        newHistory.setPositionto(response.getPosition().getPositionname());
                    }
                    newHistory.setJoincallsign(this.getDDMMYYYY());
                    newHistory.setCreatedby(employeeDao.getCreatedby());
                    newHistory.setHistorycode(employee.getHistorycode());
                    historyRepository.saveAndFlush(newHistory);
                    //end history update promote


                    return response;
                } else {
                    Employee response = new Employee();
                    response.setFullname("NIK Used, Please Choose Another NIK");
                    return response;
                }
            } else {
                Employee response = new Employee();
                response.setFullname("Callsign Used, Please Choose Another Callsign / Create New Callsign");
                return response;
            }
        }

    }

    public Employee softDeleteEmployee(Integer id, String username){
        Employee employee = this.getEmpActiveById(id);
        Status status = statusRepository.findByStatus("OFF");
        employee.setStatus(status);
        employee.setModifiedby(username);
        employee.setModifiedon(LocalDateTime.now());
        return employeeRepository.saveAndFlush(employee);
    }

    public Employee employeeResign(EmployeeDao employeeDao){
        Employee employee = this.getEmpActiveById(employeeDao.getEmployeeid());
        Status newStatus = statusRepository.findByStatus("RESIGN");
        if (!Objects.isNull(employee)){
            employee.setStatus(newStatus);
            employee.setRemark_resign(employeeDao.getRemark_resign());
            if (Objects.isNull(employeeDao.getResigndate())){
                employee.setResigndate(this.getDDMMYYYY());
            } else {
                employee.setResigndate(employeeDao.getResigndate());
            }
            return employeeRepository.saveAndFlush(employee);
        } else {
            return new Employee();
        }
    }

    public Employee replacementEmployee(EmployeeDao employeeDao){
        String historyCode = this.fixedHistoryCode();
        Employee checkCallsign = getByCallsign(employeeDao.getCallsign());
        Employee checkNIKKTP = getByNikKtp(employeeDao.getNikktp());
        Employee checkNIK = getByNIK(employeeDao.getNik());
        if (Objects.isNull(checkCallsign) && Objects.isNull(checkNIKKTP) && Objects.isNull(checkNIK)){
            LocalDateTime now = LocalDateTime.now();
            Employee newEmployee = new Employee();
            Education education = educationRepository.findByEduid(employeeDao.getEducationid());
            Regional regional = regionalRepository.getByRegionalid(employeeDao.getRegionalid());
            Division division = divisionRepository.findByDivisionid(employeeDao.getDivisionid());
            MainJob mainJob = mainjobRepository.getByMainjobid(employeeDao.getMainjobid());
            Position position = positionRepository.getByPositionid(employeeDao.getPositionid());
            Level level = levelRepository.findByLevelid(employeeDao.getLevelid());
            Company company = companyRepository.getByCompanyid(employeeDao.getCompanyid());
            Status status = statusRepository.getById(2);
            newEmployee.setEmployeeid(null);
            newEmployee.setFullname(employeeDao.getFullname());
            newEmployee.setCallsign(employeeDao.getCallsign());
            newEmployee.setAddress(employeeDao.getAddress());
            newEmployee.setNik(employeeDao.getNik());
            newEmployee.setNikktp(employeeDao.getNikktp());
            newEmployee.setEducation(education);
            newEmployee.setPhonenum(employeeDao.getPhonenum());
            newEmployee.setRegional(regional);
            newEmployee.setDivision(division);
            newEmployee.setMainJob(mainJob);
            newEmployee.setPosition(position);
            newEmployee.setLevel(level);
            newEmployee.setJoindate(employeeDao.getJoindate());
            newEmployee.setJoincallsign(employeeDao.getJoincallsign());
            newEmployee.setReleasecallsign(null);
            newEmployee.setCompany(company);
            newEmployee.setEmail(employeeDao.getEmail());
            newEmployee.setRemark(employeeDao.getRemark());
            newEmployee.setRemark_resign(null);
            newEmployee.setResigndate(null);
            newEmployee.setStatus(status);
            newEmployee.setDatecreated(now);
            newEmployee.setCreatedby(employeeDao.getCreatedby());
            newEmployee.setHistorycode(historyCode);

            //history add
            History newHistory = new History();
            newHistory.setType("NEW");
            newHistory.setCreateddate(now);
            newHistory.setJoincallsign(employeeDao.getJoincallsign());
            newHistory.setCreatedby(employeeDao.getCreatedby());
            newHistory.setHistorycode(historyCode);
            historyRepository.saveAndFlush(newHistory);
            //end history add

            //save new employee
            Employee response = employeeRepository.saveAndFlush(newEmployee);
            //end save new employee

            //update resigned employee
            Employee oldEmployee = getByid(employeeDao.getReplacementemployeeid());
            oldEmployee.setReplacementemployeeid(response.getEmployeeid());
            employeeRepository.saveAndFlush(oldEmployee);
            //end update resigned employee

            return response;
        } else {
            if(!Objects.isNull(checkCallsign)) {
                Employee employee = new Employee();
                employee.setFullname("Callsign Used, Please Use Another Callsign / Create New Callsign");
                return employee;
            } else if (!Objects.isNull(checkNIKKTP)){
                Employee employee = new Employee();
                employee.setFullname("NIK KTP Ever Registered in Database, Cannot Save Duplicate NIK KTP");
                return employee;
            } else if (!Objects.isNull(checkNIK)) {
                Employee employee = new Employee();
                employee.setFullname("NIK Registered, Cannot Save Duplicate NIK");
                return employee;
            } else {
                Employee employee = new Employee();
                employee.setFullname("Duplicate Data, Please Check Callsign / NIK KTP / NIK");
                return employee;
            }
        }
    }

    public String getDDMMYYYY () {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String newDate = formatter.format(date);
        return newDate;
    }

    private String getRandomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    public String fixedHistoryCode(){

        String code = null;
        Integer check = 0;
        do {
            code = this.getRandomString();
            Employee checkCode = getByHistoryCode(code);
            if (Objects.isNull(checkCode)){
                check = 0;
            } else {
                check = 1;
            }
        } while (check == 1);
        return code;
    }

    public List<Company> getAllCompanyName(String companyname){
        List<Company> list = new ArrayList<Company>();
        List<Company> companies = companyRepository.findAll();
        if (companyname.equals("")){
            for (int i = 0; i < companies.size(); i++){
                list.add(companies.get(i));
            }
        } else {
            Company company = companyRepository.getByCompanyname(companyname);
            list.add(company);
        }
        return list;
    }

    public List<Regional> getAllRegionalDept(String regionaldept){
        List<Regional> list = new ArrayList<Regional>();
        List<Regional> regionals = regionalRepository.findAll();
        if (regionaldept.equals("")){
            for (int i = 0; i < regionals.size(); i++){
                list.add(regionals.get(i));
            }
        } else {
            Regional regional = regionalRepository.getByRegionaldept(regionaldept);
            list.add(regional);
        }
        return list;
    }

    public List<Division> getAllDivisionName(String divisionname){
        List<Division> list = new ArrayList<Division>();
        List<Division> divisions = divisionRepository.findAll();
        if (divisionname.equals("")){
            for (int i = 0; i < divisions.size(); i++){
                list.add(divisions.get(i));
            }
        } else {
            Division division = divisionRepository.findByDivisioname(divisionname);
            list.add(division);
        }
        return list;
    }

    public List<Position> getAllPositionName(String positionname){
        List<Position> list = new ArrayList<Position>();
        List<Position> positions = positionRepository.findAll();
        if (positionname.equals("")){
            for (int i = 0; i < positions.size(); i++){
                list.add(positions.get(i));
            }
        } else {
            Position position = positionRepository.getByPositionname(positionname);
            list.add(position);
        }
        return list;
    }

    public List<EmployeeReportDao> getDataForReportEmployee(String company, String regional, String division, String position){
        List<Company> companies = this.getAllCompanyName(company);
        List<Regional> regionals = this.getAllRegionalDept(regional);
        List<Division> divisions = this.getAllDivisionName(division);
        List<Position> positions = this.getAllPositionName(position);
        Status status = statusRepository.findByStatus("ACTIVE");
        List<Employee> employees = employeeRepository.findAllByStatusAndCompanyInAndRegionalInAndDivisionInAndPositionIn(status,companies,regionals,divisions,positions);
        List<EmployeeReportDao> employeeReportDaoList = new ArrayList<EmployeeReportDao>();
        for (int i = 0; i<employees.size(); i++){
            EmployeeReportDao employee = new EmployeeReportDao();
            employee.setNo(i+1);
            employee.setEmployeeid(employees.get(i).getEmployeeid());
            employee.setFullname(employees.get(i).getFullname());
            employee.setCallsign(employees.get(i).getCallsign());
            employee.setNikktp(employees.get(i).getNikktp());
            employee.setNik(employees.get(i).getNik());
            employee.setCompanyname(employees.get(i).getCompany().getCompanyname());
            employee.setRegionaldept(employees.get(i).getRegional().getRegionaldept());
            employee.setDivisioname(employees.get(i).getDivision().getDivisioname());
            employee.setMainjobname(employees.get(i).getMainJob().getMainjobname());
            employee.setPositionname(employees.get(i).getPosition().getPositionname());
            employee.setLevelname(employees.get(i).getLevel().getLevelname());
            employee.setEmail(employees.get(i).getEmail());
            employee.setAddress(employees.get(i).getAddress());
            employee.setEducationname(employees.get(i).getEducation().getEducationname());
            employee.setPhonenum(employees.get(i).getPhonenum());
            employee.setRemark(employees.get(i).getRemark());
            employee.setRemark_resign(employees.get(i).getRemark_resign());
            employee.setJoindate(employees.get(i).getJoindate());
            employee.setResigndate(employees.get(i).getResigndate());
            employee.setJoincallsign(employees.get(i).getJoincallsign());
            employeeReportDaoList.add(employee);
        }
        return employeeReportDaoList;
    }

    public List<EmployeeReportDao> getDataForReportResignEmployee(String startdate, String enddate, String company){
        List<Company> listCompanies = this.getAllCompanyName(company);
        List<Integer> companies = new ArrayList<>();
        for (int i = 0; i < listCompanies.size(); i++){
            companies.add(listCompanies.get(i).getCompanyid());
        }
        String start = "";
        String end = "";
        if (startdate.equals("")){
            start = "01-01-1999";
        } else {
            start = startdate;
        }

        if (enddate.equals("")){
            end = this.getDDMMYYYY();
        } else {
            end = enddate;
        }
        System.out.println(start);
        System.out.println(end);
        List<Employee> employees = employeeRepository.customResignEmployeeFilters(start, end, companies);
        List<EmployeeReportDao> employeeReportDaoList = new ArrayList<EmployeeReportDao>();
        for (int i = 0; i<employees.size(); i++){
            EmployeeReportDao employee = new EmployeeReportDao();
            employee.setNo(i+1);
            employee.setEmployeeid(employees.get(i).getEmployeeid());
            employee.setFullname(employees.get(i).getFullname());
            employee.setCallsign(employees.get(i).getCallsign());
            employee.setNikktp(employees.get(i).getNikktp());
            employee.setNik(employees.get(i).getNik());
            employee.setCompanyname(employees.get(i).getCompany().getCompanyname());
            employee.setRegionaldept(employees.get(i).getRegional().getRegionaldept());
            employee.setDivisioname(employees.get(i).getDivision().getDivisioname());
            employee.setMainjobname(employees.get(i).getMainJob().getMainjobname());
            employee.setPositionname(employees.get(i).getPosition().getPositionname());
            employee.setLevelname(employees.get(i).getLevel().getLevelname());
            employee.setEmail(employees.get(i).getEmail());
            employee.setAddress(employees.get(i).getAddress());
            employee.setEducationname(employees.get(i).getEducation().getEducationname());
            employee.setPhonenum(employees.get(i).getPhonenum());
            employee.setRemark(employees.get(i).getRemark());
            employee.setRemark_resign(employees.get(i).getRemark_resign());
            employee.setJoindate(employees.get(i).getJoindate());
            employee.setResigndate(employees.get(i).getResigndate());
            employee.setJoincallsign(employees.get(i).getJoincallsign());
            employeeReportDaoList.add(employee);
        }
        return employeeReportDaoList;
    }




}
