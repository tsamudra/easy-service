package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.EducationDao;
import io.easy.easyservice.models.entity.Education;
import io.easy.easyservice.services.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class EducationController {

    @Autowired
    private EducationService educationService;

    @GetMapping("/getalledu")
    public List<Education> getAll(){
        return educationService.getAllEdu();
    }

    @GetMapping("/getedubyid")
    public Education getEdubyId(@RequestParam Integer id) {
        return educationService.getEdubyId(id);
    }

    @GetMapping("/getedubyname")
    public Education getEdubyName(@RequestParam String eduname){
        return educationService.getEdubyName(eduname);
    }

    @PostMapping("/createedu")
    public Education createEdu(@RequestBody EducationDao educationDao){
        return educationService.createNewEducation(educationDao);
    }

    @PatchMapping("/updateedu")
    public Education updateEdu(@RequestBody EducationDao educationDao){
        return educationService.updateEducation(educationDao);
    }

    @DeleteMapping("/deleteedu")
    public Integer deleteEdu(@RequestParam Integer id){
        return educationService.deleteEducation(id);
    }


}
