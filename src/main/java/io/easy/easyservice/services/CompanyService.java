package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.CompanyDao;
import io.easy.easyservice.models.entity.Company;
import io.easy.easyservice.models.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public List<Company> getAllCompany(){
        return companyRepository.findAll();
    }

    public Company getCompanybyID(Integer id) {
        return companyRepository.getByCompanyid(id);
    }

    public Company getCompanybyName(String companyname) {
        return companyRepository.getByCompanyname(companyname);
    }

    public Company createNewCompany(CompanyDao companyDao){
        Company checkAvailability = getCompanybyName(companyDao.getCompanyname());
        if(Objects.isNull(checkAvailability)){
            LocalDateTime now = LocalDateTime.now();
            Company tempcompany = new Company();
            tempcompany.setCompanyid(null);
            tempcompany.setCompanyname(companyDao.getCompanyname());
            tempcompany.setDatecreated(now);
            return companyRepository.saveAndFlush(tempcompany);
        } else {
            Company response = new Company();
            response.setCompanyname("Company Name Used. Cannot Save Duplicate Data");
            return response;
        }
    }

    public Company updateCompanyDetails(CompanyDao companyDao) {
        Company companyFromDB = getCompanybyID(companyDao.getCompanyid());
        Company checkCompanyName = getCompanybyName(companyDao.getCompanyname());
        if (!Objects.isNull(companyFromDB)){
            if (companyFromDB.getCompanyname().equals(companyDao.getCompanyname())){
                companyFromDB.setCompanyname(companyDao.getCompanyname());
                return companyRepository.saveAndFlush(companyFromDB);
            } else {
                if (Objects.isNull(checkCompanyName)){
                    companyFromDB.setCompanyname(companyDao.getCompanyname());
                    return companyRepository.saveAndFlush(companyFromDB);
                }else{
                    Company response = new Company();
                    response.setCompanyname("Company Name Used. Cannot Save Duplicate Data");
                    return response;
                }
            }
        } else {
            return new Company();
        }
    }

    public Integer deleteCompany(Integer id) {
        return companyRepository.removeByCompanyid(id);
    }

}
