package io.easy.easyservice.controller;

import io.easy.easyservice.models.entity.History;
import io.easy.easyservice.services.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/history")
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    @GetMapping("/getallhistory")
    public List<History> getAllByCode(@RequestParam String code) {
        return historyService.getAllByCode(code);
    }
}
