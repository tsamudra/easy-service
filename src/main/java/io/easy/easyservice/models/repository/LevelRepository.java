package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Level;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface LevelRepository extends JpaRepository<Level, Integer> {
    Level findByLevelid(Integer id);
    Level findByLevelname(String level);
    @Transactional
    Integer removeByLevelid(Integer id);
}
