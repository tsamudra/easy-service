package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.RegionalDao;
import io.easy.easyservice.models.entity.Regional;
import io.easy.easyservice.services.RegionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class RegionalController {

    @Autowired
    private RegionalService regionalService;

    @GetMapping("/getallregional")
    public List<Regional> getAllRegional(){
        return regionalService.getAll();
    }

    @GetMapping("/getregionalbyid")
    public Regional getById(@RequestParam Integer id) {
        return regionalService.getById(id);
    }

    @GetMapping("/getregionalbyname")
    public Regional getByName(@RequestParam String regname){
        return regionalService.getByName(regname);
    }

    @PostMapping("/createregional")
    public Regional createRegional(@RequestBody RegionalDao regionalDao){
        return regionalService.createNew(regionalDao);
    }

    @PatchMapping("/updateregional")
    public Regional updateRegional(@RequestBody RegionalDao regionalDao){
        return regionalService.updateRegional(regionalDao);
    }

    @DeleteMapping("/deleteregional")
    public Integer deleteRegional(@RequestParam Integer id) {
        return regionalService.deleteRegional(id);
    }

}
