package io.easy.easyservice.services;

import io.easy.easyservice.models.entity.Status;
import io.easy.easyservice.models.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusService {

    @Autowired
    private StatusRepository statusRepository;

    public List<Status> getAllBy(String code){
        return statusRepository.findAllByStatuscode(code);
    }

    public Status getByStatus(String status){
        return statusRepository.findByStatus(status);
    }
}
