package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.DivisionDao;
import io.easy.easyservice.models.entity.Division;
import io.easy.easyservice.services.DivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class DivisionController {

    @Autowired
    private DivisionService divisionService;

    @GetMapping("/getalldivision")
    public List<Division> getAll(){
        return divisionService.getAll();
    }

    @GetMapping("/getdivisionbyid")
    public Division getById(@RequestParam Integer id){
        return divisionService.getById(id);
    }

    @GetMapping("/getdivisionbyname")
    public Division getByName(@RequestParam String hubname){
        return divisionService.getByName(hubname);
    }

    @PostMapping("/createdivision")
    public Division createHub(@RequestBody DivisionDao divisionDao){
        return divisionService.createHub(divisionDao);
    }

    @PatchMapping("/updatedivision")
    public Division updateHub(@RequestBody DivisionDao divisionDao){
        return divisionService.updateHub(divisionDao);
    }

    @DeleteMapping("/deletedivision")
    public Integer deleteHub(@RequestParam Integer id){
        return divisionService.deleteHub(id);
    }



}
