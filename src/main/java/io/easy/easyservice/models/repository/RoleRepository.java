package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role getByRolename(String rolename);
    Role getByRoleid(Integer id);
    @Transactional
    Integer removeByRoleid(Integer id);
}
