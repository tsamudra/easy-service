package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.History;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoryRepository extends JpaRepository<History, Integer> {
    List<History> findAllByHistorycode(String code);
}
