package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Level;
import io.easy.easyservice.models.entity.LevelList;
import io.easy.easyservice.models.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface LevelListRepository extends JpaRepository<LevelList, Integer> {

    LevelList findByLevellistid(Integer id);
    LevelList findByPositionAndLevel(Position position, Level level);
    @Transactional
    Integer removeByLevellistid(Integer id);
}
