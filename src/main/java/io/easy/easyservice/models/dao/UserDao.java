package io.easy.easyservice.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDao {
    private Integer userid;
    private Integer roleid;
    private Integer companyid;
    private Integer regionalid;
    private String username;
    private String password;
    private String fullname;
}
