package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.PositionDao;
import io.easy.easyservice.models.entity.MainJob;
import io.easy.easyservice.models.entity.Position;
import io.easy.easyservice.models.repository.MainjobRepository;
import io.easy.easyservice.models.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class PositionService {

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private MainjobRepository mainjobRepository;

    public List<Position> getAll(){
        return positionRepository.findAll();
    }

    public Position getById(Integer id){
        return positionRepository.getByPositionid(id);
    }

    public Position getByName(String postname){
        return positionRepository.getByPositionname(postname);
    }

    public Position getByRecord(PositionDao positionDao){
        MainJob mainJob = mainjobRepository.getByMainjobid(positionDao.getMainjobid());
        return positionRepository.getByMainJobAndPositionname(mainJob, positionDao.getPositionname());
    }

    public Position createPosition(PositionDao positionDao){
        Position checkRecord = getByRecord(positionDao);
        if(Objects.isNull(checkRecord)){
            LocalDateTime now = LocalDateTime.now();
            MainJob tempjob = mainjobRepository.getByMainjobid(positionDao.getMainjobid());
            Position tempPost = new Position();
            tempPost.setPositionid(null);
            tempPost.setMainJob(tempjob);
            tempPost.setPositionname(positionDao.getPositionname());
            tempPost.setDatecreated(now);
            return positionRepository.saveAndFlush(tempPost);
        } else {
            Position response = new Position();
            response.setPositionname("Record Registered. Cannot Save ");
            return response;
        }
    }

    public Position updatePosition(PositionDao positionDao){
        Position postFromDB = getById(positionDao.getPositionid());
        Position checkRecord = getByRecord(positionDao);
        if (!Objects.isNull(postFromDB)){
            if (postFromDB.getMainJob().getMainjobid().equals(positionDao.getMainjobid()) && postFromDB.getPositionname().equals(positionDao.getPositionname())) {
                MainJob tempjob = mainjobRepository.getByMainjobid(positionDao.getMainjobid());
                postFromDB.setMainJob(tempjob);
                postFromDB.setPositionname(positionDao.getPositionname());
                return positionRepository.saveAndFlush(postFromDB);
            } else {
                if (Objects.isNull(checkRecord)) {
                    MainJob tempjob = mainjobRepository.getByMainjobid(positionDao.getMainjobid());
                    postFromDB.setMainJob(tempjob);
                    postFromDB.setPositionname(positionDao.getPositionname());
                    return positionRepository.saveAndFlush(postFromDB);
                } else {
                    Position response = new Position();
                    response.setPositionname("Record Registered. Cannot Save ");
                    return response;
                }
            }
        } else {
            return new Position();
        }
    }

    public Integer deletePosition(Integer id){
        return positionRepository.removeByPositionid(id);
    }
}
