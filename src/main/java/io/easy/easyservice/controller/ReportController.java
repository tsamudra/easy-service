package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.DownloadParamsDao;
import io.easy.easyservice.models.dao.ReportDao;
import io.easy.easyservice.models.dao.UploadFileDao;
import io.easy.easyservice.models.dao.UploadResponseDao;
import io.easy.easyservice.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/master")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @PostMapping("/gettemplate")
    public void exportTemplate(HttpServletRequest request, HttpServletResponse response, @RequestBody ReportDao reportDao){
        reportService.getTemplate(request, response, reportDao);
    }

    @PostMapping(value = "/uploadtemplate", consumes = { "multipart/form-data" })
    public UploadResponseDao receiveTemplate(@ModelAttribute UploadFileDao uploadFileDao){
        return reportService.receiveFile(uploadFileDao);
    }

    @PostMapping("/getemployeereport")
    public void exportReportEmployee(HttpServletRequest request, HttpServletResponse response, @RequestBody DownloadParamsDao downloadParamsDao){
        reportService.getExcelReportEmployee(request, response, downloadParamsDao.getCompany(), downloadParamsDao.getRegional(), downloadParamsDao.getDivision(), downloadParamsDao.getPosition());
    }

    @PostMapping("/getemployeeresignreport")
    public void exportReportEmployeeResign(HttpServletRequest request, HttpServletResponse response, @RequestBody DownloadParamsDao downloadParamsDao){
        System.out.println(downloadParamsDao);
        reportService.getExcelReportResignEmployee(request, response, downloadParamsDao.getStartdate(), downloadParamsDao.getEnddate(), downloadParamsDao.getCompany());
    }

}
