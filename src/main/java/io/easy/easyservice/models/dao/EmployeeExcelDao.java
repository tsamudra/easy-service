package io.easy.easyservice.models.dao;

import io.easy.easyservice.models.entity.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeExcelDao {

    private Integer employeeid;
    private String callsign;
    private String nikktp;
    private String nik;
    private String fullname;
    private Company company;
    private Regional regional;
    private Division division;
    private MainJob mainJob;
    private Position position;
    private Level level;
    private Education education;
    private String joindate;
    private String resigndate;
    private String joincallsign;
    private String releasecallsign;
    private String address;
    private String email;
    private String phonenum;
    private String remark;
    private String remark_resign;
    private String remark_promote;
    private String remark_movement;
    private String remark_inactive;
    private Status status;
    private LocalDateTime datecreated;
    private LocalDateTime modifiedon;
    private String createdby;
    private String modifiedby;
    private String historycode;
}
