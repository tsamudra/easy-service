package io.easy.easyservice.models.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "easy_user")
@Data
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "roleid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Role role;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "companyid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "regionalid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Regional regional;

    private String username;
    @Column(length = 600)
    private String password;
    private String fullname;
    private LocalDateTime accountcreated;

}
