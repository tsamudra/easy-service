package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.MainjobDao;
import io.easy.easyservice.models.entity.MainJob;
import io.easy.easyservice.services.MainjobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class MainJobCotroller {

    @Autowired
    private MainjobService mainjobService;

    @GetMapping("/getallmainjob")
    public List<MainJob> getAll(){
        return mainjobService.getAllMainjob();
    }

    @GetMapping("/getmainjobbyid")
    public MainJob getById (@RequestParam Integer id){
        return mainjobService.getByid(id);
    }

    @GetMapping("/getmainjobbyname")
    public MainJob getByName (@RequestParam String mainjobname){
        return mainjobService.getByName(mainjobname);
    }

    @PostMapping("/createnewmainjob")
    public MainJob createNewMainjob(@RequestBody MainjobDao mainjobDao) {
        return mainjobService.createNewMainjob(mainjobDao);
    }

    @PatchMapping("/updatemainjob")
    public MainJob updateMainjob(@RequestBody MainjobDao mainjobDao) {
        return mainjobService.updateMainjob(mainjobDao);
    }

    @DeleteMapping("/deletemainjob")
    public Integer deleteMainjob(@RequestParam Integer id) {
        return mainjobService.deleteMainjob(id);
    }


}
