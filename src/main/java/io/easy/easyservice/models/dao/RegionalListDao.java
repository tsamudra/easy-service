package io.easy.easyservice.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegionalListDao {
    private Integer regionalistid;
    private Integer regionalid;
    private Integer divisionid;
}
