package io.easy.easyservice.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegionalDao {
    private Integer regionalid;
    private String regionaldept;
}
