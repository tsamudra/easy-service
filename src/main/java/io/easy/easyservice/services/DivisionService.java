package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.DivisionDao;
import io.easy.easyservice.models.entity.Division;
import io.easy.easyservice.models.entity.Employee;
import io.easy.easyservice.models.entity.Regional;
import io.easy.easyservice.models.repository.DivisionRepository;
import io.easy.easyservice.models.repository.EmployeeRepository;
import io.easy.easyservice.models.repository.RegionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class DivisionService {

    @Autowired
    private DivisionRepository divisionRepository;

    @Autowired
    private RegionalRepository regionalRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Division> getAll(){
        return divisionRepository.findAll();
    }

    public Division getById(Integer id){
        return divisionRepository.findByDivisionid(id);
    }

    public Division getByName(String hubname){
        return divisionRepository.findByDivisioname(hubname);
    }

    public Division createHub(DivisionDao divisionDao){
        Division checkAvailability = getByName(divisionDao.getDivisionname());
        if (Objects.isNull(checkAvailability)){
            LocalDateTime now = LocalDateTime.now();
            Division tempDivision = new Division();
            tempDivision.setDivisionid(null);
            tempDivision.setDivisioname(divisionDao.getDivisionname());
            tempDivision.setDatecreated(now);
            return divisionRepository.saveAndFlush(tempDivision);
        } else {
            Division response = new Division();
            response.setDivisioname("Division Name Used, Cannot Save Duplicate Division Name");
            return response;
        }
    }

    public Division updateHub(DivisionDao divisionDao){
        Division divisionFromDB = getById(divisionDao.getDivisionid());
        Division checkDivisionName = getByName(divisionDao.getDivisionname());
        if(!Objects.isNull(divisionFromDB)){
           if(divisionFromDB.getDivisioname().equals(divisionDao.getDivisionname())){
               divisionFromDB.setDivisioname(divisionDao.getDivisionname());
               return divisionRepository.saveAndFlush(divisionFromDB);
           } else {
               if(Objects.isNull(checkDivisionName)){
                   divisionFromDB.setDivisioname(divisionDao.getDivisionname());
                   return divisionRepository.saveAndFlush(divisionFromDB);
               } else {
                   Division response = new Division();
                   response.setDivisioname("Division Name Used, Cannot Save Duplicate Division Name");
                   return response;
               }
           }
        } else {
            return new Division();
        }
    }

    public Integer deleteHub(Integer id){
        List<Employee> divisions = employeeRepository.findAllByDivision_Divisionid(id);
        if (ObjectUtils.isEmpty(divisions)){
            return 0;
        } else {
            return divisionRepository.removeByDivisionid(id);
        }
    }

}
