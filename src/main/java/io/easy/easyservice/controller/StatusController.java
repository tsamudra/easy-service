package io.easy.easyservice.controller;

import io.easy.easyservice.models.entity.Status;
import io.easy.easyservice.services.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class StatusController {

    @Autowired
    private StatusService statusService;

    @GetMapping("/getallstatus")
    public List<Status> getAllBy(@RequestParam String code){
        return statusService.getAllBy(code);
    }

}
