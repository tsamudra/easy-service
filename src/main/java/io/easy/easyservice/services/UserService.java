package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.UserDao;
import io.easy.easyservice.models.entity.Company;
import io.easy.easyservice.models.entity.Regional;
import io.easy.easyservice.models.entity.Role;
import io.easy.easyservice.models.entity.User;
import io.easy.easyservice.models.repository.CompanyRepository;
import io.easy.easyservice.models.repository.RegionalRepository;
import io.easy.easyservice.models.repository.RoleRepository;
import io.easy.easyservice.models.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private RegionalRepository regionalRepository;

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public User getUserDetailsbyUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User getUserDetailsbyId(Integer id) {
        return userRepository.findByUserid(id);
    }

    public User createNewAccount (UserDao user) {
        //check username
        User checkAvailability = getUserDetailsbyUsername(user.getUsername());
        if (Objects.isNull(checkAvailability)){
            //username available
            LocalDateTime now = LocalDateTime.now();
            Role temprole = roleRepository.getByRoleid(user.getRoleid());
            Company tempcomp = companyRepository.getByCompanyid(user.getCompanyid());
            Regional tempreg = regionalRepository.getByRegionalid(user.getRegionalid());
            User tempuser = new User();
            tempuser.setUserid(null);
            tempuser.setRole(temprole);
            tempuser.setCompany(tempcomp);
            tempuser.setRegional(tempreg);
            tempuser.setUsername(user.getUsername());
            tempuser.setPassword(passwordEncoder.encode(user.getPassword()));
            tempuser.setFullname(user.getFullname());
            tempuser.setAccountcreated(now);
            //return new account
            return userRepository.saveAndFlush(tempuser);
        } else {
            //username used
            User tempuser = new User();
            tempuser.setFullname("Username Used, Please Choose Anothe Username");
            //return empty User
            return tempuser;
        }
    }

    public User updateAccountDetails (UserDao user) {
        User userFromDB = getUserDetailsbyId(user.getUserid());
        User checkUsername = getUserDetailsbyUsername(user.getUsername());
        if(!Objects.isNull(userFromDB)){
            if(Objects.isNull(checkUsername)){
                Role temprole = roleRepository.getByRoleid(user.getRoleid());
                Company tempcomp = companyRepository.getByCompanyid(user.getCompanyid());
                Regional tempreg = regionalRepository.getByRegionalid(user.getRegionalid());
                userFromDB.setRole(temprole);
                userFromDB.setCompany(tempcomp);
                userFromDB.setRegional(tempreg);
                userFromDB.setUsername(user.getUsername());
                userFromDB.setFullname(user.getFullname());
                return userRepository.saveAndFlush(userFromDB);
            } else {
                if (userFromDB.getUsername().equals(user.getUsername())){
                    Role temprole = roleRepository.getByRoleid(user.getRoleid());
                    Company tempcomp = companyRepository.getByCompanyid(user.getCompanyid());
                    Regional tempreg = regionalRepository.getByRegionalid(user.getRegionalid());
                    userFromDB.setRole(temprole);
                    userFromDB.setCompany(tempcomp);
                    userFromDB.setRegional(tempreg);
                    userFromDB.setUsername(user.getUsername());
                    userFromDB.setFullname(user.getFullname());
                    return userRepository.saveAndFlush(userFromDB);
                } else {
                    User response = new User();
                    response.setFullname("Username Used, Please Use Another Username");
                    return response;
                }
            }
        } else {
            return new User();
        }
    }

    public User updateAccountPassword (UserDao user) {
        User userFromDB = getUserDetailsbyId(user.getUserid());
        if(!Objects.isNull(userFromDB)){
            userFromDB.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.saveAndFlush(userFromDB);
        } else {
            return new User();
        }
    }

    public Integer deleteAccount (Integer id) {
        return userRepository.deleteUserByUserid(id);
    }

}
