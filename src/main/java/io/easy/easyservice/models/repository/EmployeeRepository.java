package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    List<Employee> findAllByStatusAndCompanyInAndRegionalInAndDivisionInAndPositionIn(Status status, List<Company> companies, List<Regional> regionals, List<Division> divisions, List<Position> positions);

    List<Employee> findAllByStatus(Status status);

    List<Employee> findAllByEmployeeidIn(List<Integer> ids);

    Employee getByEmployeeid(Integer id);

    Employee getByEmployeeidAndStatus(Integer id, Status status);

    Employee getByNikktp(String nik);

    Employee getByNikAndStatus(String nik, Status status);

    Employee getByHistorycode(String code);

    Employee getByCallsignAndStatus(String callsign, Status status);

    @Transactional
    Integer removeByEmployeeid(Integer id);

    @Query(value = "" +
            "SELECT " +
            "* " +
            "FROM " +
            "   employee " +
            "WHERE " +
            "   statusid = '1' " +
            "AND " +
            "   (resigndate >= :start AND resigndate <= :end) " +
            "AND " +
            "   companyid in (:companies)", nativeQuery = true)
    List<Employee> customResignEmployeeFilters(@Param("start") String start, @Param("end") String end, @Param("companies") List<Integer> companies);

    @Query(value = "" +
            "SELECT " +
            "* " +
            "FROM " +
            "   employee " +
            "WHERE " +
            "   statusid = '4' " +
            "AND " +
            "   (joindate >= :start AND joindate <= :end)", nativeQuery = true)
    List<Employee> customOffEmployeeFilters(@Param("start") String start, @Param("end") String end);

    List<Employee> findAllByDivision_Divisionid(Integer id);



}