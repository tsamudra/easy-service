package io.easy.easyservice.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthenticateResponse {
    private final String jwt;
    private final Integer userid;
    private final Integer roleid;
    private final Integer companyid;
    private final Integer regionalid;
    private final String username;
    private final String fullname;
}
