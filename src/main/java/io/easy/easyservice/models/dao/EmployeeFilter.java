package io.easy.easyservice.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeFilter {
    private List<String> companies;
    private List<String> regionals;
    private List<String> divisions;
    private List<String> positions;
}
