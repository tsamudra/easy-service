package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.UserDao;
import io.easy.easyservice.models.entity.User;
import io.easy.easyservice.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getusers")
    public List<User> getAllUser() {
        return userService.getAllUsers();
    }

    @GetMapping("/getuserbyusername")
    public User getUserDetailsbyUsername(@RequestParam String username){
        return userService.getUserDetailsbyUsername(username);
    }

    @GetMapping("/useruserbyid")
    public User getUserDetailsbyUserid(@RequestParam Integer id) {
        return userService.getUserDetailsbyId(id);
    }

    @PostMapping("/createuser")
    public User saveNewAccount(@RequestBody UserDao user){
        return userService.createNewAccount(user);
    }

    @DeleteMapping("/deleteuser")
    public Integer deleteUserAccount(@RequestParam Integer id) {
        return userService.deleteAccount(id);
    }

    @PatchMapping("/updateuser")
    public User updateAccount(@RequestBody UserDao user) {
        return userService.updateAccountDetails(user);
    }

    @PatchMapping("/updateuserpassword")
    public User updatePassword(@RequestBody UserDao user) {
        return userService.updateAccountPassword(user);
    }

}
