package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.RegionalListDao;
import io.easy.easyservice.models.entity.Division;
import io.easy.easyservice.models.entity.Regional;
import io.easy.easyservice.models.entity.RegionalList;
import io.easy.easyservice.models.repository.DivisionRepository;
import io.easy.easyservice.models.repository.RegionalListRepository;
import io.easy.easyservice.models.repository.RegionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class RegionalListService {

    @Autowired
    private RegionalListRepository regionalListRepository;

    @Autowired
    private RegionalRepository regionalRepository;

    @Autowired
    private DivisionRepository divisionRepository;

    public List<RegionalList> getAll(){
        return regionalListRepository.findAll();
    }

    public RegionalList getById(Integer id){
        return regionalListRepository.findByRegionallistid(id);
    }

    public RegionalList getByRecord(Integer regionalid, Integer divisionid){
        Regional regional = regionalRepository.getByRegionalid(regionalid);
        Division division = divisionRepository.findByDivisionid(divisionid);
        return regionalListRepository.findByRegionalAndDivision(regional, division);
    }

    public RegionalList createRegionalList(RegionalListDao regionalListDao){
        RegionalList checkAvailability = getByRecord(regionalListDao.getRegionalid(), regionalListDao.getDivisionid());
        if (Objects.isNull(checkAvailability)){
            LocalDateTime now = LocalDateTime.now();
            RegionalList tempRegionalList = new RegionalList();
            Regional regionaldata = regionalRepository.getByRegionalid(regionalListDao.getRegionalid());
            Division divisiondata = divisionRepository.findByDivisionid(regionalListDao.getDivisionid());
            tempRegionalList.setRegionallistid(null);
            tempRegionalList.setRegional(regionaldata);
            tempRegionalList.setDivision(divisiondata);
            tempRegionalList.setDatecreated(now);
            return regionalListRepository.saveAndFlush(tempRegionalList);
        } else {
            RegionalList response = new RegionalList();
            response.setRegionallistid(1);
            return response;
        }
    }

    public RegionalList updateRegionalList(RegionalListDao regionalListDao){
        RegionalList regionalListFromDB = getById(regionalListDao.getRegionalistid());
        RegionalList checkRecord = getByRecord(regionalListDao.getRegionalid(), regionalListDao.getDivisionid());
        if (!Objects.isNull(regionalListFromDB)){
            if (regionalListFromDB.getRegional().getRegionalid().equals(regionalListDao.getRegionalistid()) && regionalListFromDB.getDivision().getDivisionid().equals(regionalListDao.getDivisionid())){
                Regional regionaldata = regionalRepository.getByRegionalid(regionalListDao.getRegionalid());
                Division divisiondata = divisionRepository.findByDivisionid(regionalListDao.getDivisionid());
                regionalListFromDB.setRegional(regionaldata);
                regionalListFromDB.setDivision(divisiondata);
                return regionalListRepository.saveAndFlush(regionalListFromDB);
            } else {
                if (Objects.isNull(checkRecord)){
                    Regional regionaldata = regionalRepository.getByRegionalid(regionalListDao.getRegionalid());
                    Division divisiondata = divisionRepository.findByDivisionid(regionalListDao.getDivisionid());
                    regionalListFromDB.setRegional(regionaldata);
                    regionalListFromDB.setDivision(divisiondata);
                    return regionalListRepository.saveAndFlush(regionalListFromDB);
                } else {
                    RegionalList response = new RegionalList();
                    response.setRegionallistid(1);
                    return response;
                }
            }

        } else {
            return new RegionalList();
        }
    }

    public Integer deleteRegionalList(Integer id){
        return regionalListRepository.removeByRegionallistid(id);
    }

}
