package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Division;
import io.easy.easyservice.models.entity.Regional;
import io.easy.easyservice.models.entity.RegionalList;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface RegionalListRepository extends JpaRepository<RegionalList, Integer> {
    RegionalList findByRegionallistid(Integer id);
    RegionalList findByRegionalAndDivision(Regional regional, Division division);
    @Transactional
    Integer removeByRegionallistid(Integer id);
}
