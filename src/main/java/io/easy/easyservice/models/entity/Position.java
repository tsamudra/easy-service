package io.easy.easyservice.models.entity;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "position")
@Data
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer positionid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mainjobid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private MainJob mainJob;

    private String positionname;
    private LocalDateTime datecreated;

}
