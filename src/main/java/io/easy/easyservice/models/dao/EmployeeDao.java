package io.easy.easyservice.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDao {
    private Integer employeeid;
    private String callsign;
    private String nikktp;
    private String nik;
    private String fullname;
    private Integer companyid;
    private Integer regionalid;
    private Integer divisionid;
    private Integer mainjobid;
    private Integer positionid;
    private Integer levelid;
    private Integer educationid;
    private String joindate;
    private String resigndate;
    private String joincallsign;
    private String releasecallsign;
    private String address;
    private String email;
    private String phonenum;
    private String remark;
    private String remark_resign;
    private String remark_promote;
    private String remark_movement;
    private String remark_inactive;
    private String statusid;
    private String createdby;
    private Integer replacementemployeeid;

}
