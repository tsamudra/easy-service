package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.*;
import io.easy.easyservice.models.entity.*;
import io.easy.easyservice.models.repository.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@Service
public class ReportService {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private RegionalService regionalService;

    @Autowired
    private DivisionService divisionService;

    @Autowired
    private MainjobService mainjobService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private EducationService educationService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String[] HEADERs = {"employeeid", "address", "callsign", "fullname", "joindate", "nik", "phonenum", "resigndate", "companyid", "educationid", "mainjobid", "positionid", "regionalid", "statusid", "datecreated", "email", "joincallsign", "releasecallsign", "remark" , "remark_movement", "remark_promote", "remark_resign", "levelid", "nikktp", "createdby", "divisionid", "historycode", "remark_inactive", "modifiedby", "modifiedon"};
    static String SHEET = "template";

    public void getTemplate(HttpServletRequest request, HttpServletResponse response, ReportDao reportDao){
        response.setContentType("application/msexcel");
        response.setHeader("Content-Disposition", "attachment; filename=\"TestDown.xlsx\"");
        try {
            InputStream io = new ClassPathResource("templates/multitemplate.xlsx").getInputStream();
            OutputStream os = response.getOutputStream();

            //list for master data
            List<Company> companies = companyService.getAllCompany();
            List<Regional> regionals = regionalService.getAll();
            List<Division> divisions = divisionService.getAll();
            List<MainJob> mainjobs = mainjobService.getAllMainjob();
            List<Position> positions = positionService.getAll();
            List<Level> levels = levelService.getAll();
            List<Status> status = statusService.getAllBy("EMP_STATUS");
            List<Education> educations = educationService.getAllEdu();
            //end list for master data

            //list for employees data
            List<ReportEmployeeDao> employees = this.getAllEmployee(reportDao);
            //end list for employees data

            Context context = new Context();

            //set params
            context.putVar("StatusList", status);
            context.putVar("EducationList", educations);
            context.putVar("CompanyList", companies);
            context.putVar("RegionalList", regionals);
            context.putVar("DivisionList", divisions);
            context.putVar("MainJobList", mainjobs);
            context.putVar("PositionList", positions);
            context.putVar("LevelList", levels);
            context.putVar("EmployeeList", employees);
            //end set params

            JxlsHelper.getInstance().processTemplate(io, os, context);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private List<ReportEmployeeDao> getAllEmployee(ReportDao reportDao) {
        List<Employee> employees = employeeRepository.findAllByEmployeeidIn(reportDao.getIds());
        List<ReportEmployeeDao> newList = new ArrayList<>();
        if(Objects.isNull(reportDao.getIds().toArray())){
            return newList;
        } else {
            for (int i = 0; i<employees.size(); i++){
                newList.add(new ReportEmployeeDao(
                        employees.get(i).getEmployeeid(),
                        employees.get(i).getCallsign(),
                        employees.get(i).getNikktp(),
                        employees.get(i).getNik(),
                        employees.get(i).getFullname(),
                        employees.get(i).getCompany().getCompanyname(),
                        employees.get(i).getRegional().getRegionaldept(),
                        employees.get(i).getDivision().getDivisioname(),
                        employees.get(i).getMainJob().getMainjobname(),
                        employees.get(i).getPosition().getPositionname(),
                        employees.get(i).getLevel().getLevelname(),
                        employees.get(i).getEducation().getEducationname(),
                        employees.get(i).getJoindate(),
                        employees.get(i).getResigndate(),
                        employees.get(i).getJoincallsign(),
                        employees.get(i).getReleasecallsign(),
                        employees.get(i).getAddress(),
                        employees.get(i).getEmail(),
                        employees.get(i).getPhonenum(),
                        employees.get(i).getRemark(),
                        employees.get(i).getRemark_resign(),
                        employees.get(i).getRemark_promote(),
                        employees.get(i).getRemark_movement(),
                        employees.get(i).getRemark_inactive(),
                        employees.get(i).getStatus().getStatus(),
                        employees.get(i).getCreatedby()
                ));
            }
        }
        return newList;
    }

    public UploadResponseDao receiveFile(UploadFileDao uploadFileDao){
        try {
            UploadResponseDao response = new UploadResponseDao();
            Boolean check = this.checkExcel(uploadFileDao.getFile());
            if (check == false) {
                response.setMessage("File Not Supported");
                response.setStatus("FAILED");
                return response;
            } else {
                //read excel data
                List<EmployeeExcelDao> employees = this.excelToList(uploadFileDao.getFile().getInputStream());
                //end read excel data

                // remove null element in list
                Integer size = employees.size();
                employees.remove(size - 1);
                employees.remove(size - 2);
                // end remove null element in list


                //data validation
                CheckExcelDao result = this.checkAllExcelData(employees, uploadFileDao.getUsername());
                //end data validation

                System.out.println(result);
                if (result.getErrorCount().equals(0)){
                    //save all data
                    CheckExcelDao saved = this.saveAllData(employees, uploadFileDao.getUsername());
                    response.setMessage("All Datas Saved");
                    response.setStatus("OK");
                    return response;
                } else {
                    response.setMessage(result.getErrorCount() + " Rows of Data Detected Are Not Eligible. Please Check Again.");
                    response.setStatus("FAILED");
                    return response;
                }

            }
        } catch (Exception e){
            throw new RuntimeException("error : " + e.getMessage());
        }
    }

    private List<EmployeeExcelDao> excelToList(InputStream is) {
        try {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet =workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<EmployeeExcelDao> datas = new ArrayList<EmployeeExcelDao>();
            Integer i = 0;

            while (rows.hasNext()){
                Row currentRow = rows.next();

                //headers
                if ( (i.equals(0)) || (i.equals(1))){
                    i++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                EmployeeExcelDao employeeExcelDao = new EmployeeExcelDao();

                Integer j = 0;

                while (cellsInRow.hasNext()){
                    Cell currentCell = cellsInRow.next();
                    switch (j){
                        case 0:
                            Double num = new Double(currentCell.getNumericCellValue());
                            int value = num.intValue();
                            if(Objects.isNull(value)){
                                employeeExcelDao.setEmployeeid(null);
                            } else {
                                employeeExcelDao.setEmployeeid(value);
                            }
                            break;

                        case 1:
                            employeeExcelDao.setCallsign(currentCell.getStringCellValue());
                            break;

                        case 2:
                            employeeExcelDao.setNikktp(currentCell.getStringCellValue());
                            break;

                        case 3:
                            employeeExcelDao.setNik(currentCell.getStringCellValue());
                            break;

                        case 4:
                            employeeExcelDao.setFullname(currentCell.getStringCellValue());
                            break;

                        case 5:
                            Company company = companyService.getCompanybyName(currentCell.getStringCellValue());
                            employeeExcelDao.setCompany(company);
                            break;

                        case 6:
                            Regional regional = regionalService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setRegional(regional);
                            break;

                        case 7:
                            Division division = divisionService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setDivision(division);
                            break;

                        case 8:
                            MainJob mainJob = mainjobService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setMainJob(mainJob);
                            break;

                        case 9:
                            Position position = positionService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setPosition(position);
                            break;

                        case 10:
                            Level level = levelService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setLevel(level);
                            break;

                        case 11:
                            employeeExcelDao.setEmail(currentCell.getStringCellValue());
                            break;

                        case 12:
                            employeeExcelDao.setAddress(currentCell.getStringCellValue());
                            break;

                        case 13:
                            Education education = educationService.getEdubyName(currentCell.getStringCellValue());
                            employeeExcelDao.setEducation(education);
                            break;

                        case 14:
                            employeeExcelDao.setPhonenum(currentCell.getStringCellValue());
                            break;

                        case 15:
                            employeeExcelDao.setJoindate(currentCell.getStringCellValue());
                            break;

                        case 16:
                            employeeExcelDao.setResigndate(currentCell.getStringCellValue());
                            break;

                        case 17:
                            employeeExcelDao.setRemark(currentCell.getStringCellValue());
                            break;

                        case 18:
                            employeeExcelDao.setRemark_promote(currentCell.getStringCellValue());
                            break;

                        case 19:
                            employeeExcelDao.setRemark_movement(currentCell.getStringCellValue());
                            break;

                        case 20:
                            employeeExcelDao.setRemark_inactive(currentCell.getStringCellValue());
                            break;

                        case 21:
                            Status status = statusService.getByStatus(currentCell.getStringCellValue());
                            employeeExcelDao.setStatus(status);
                            break;

                        default:
                            break;
                    }
                    j++;
                }
                datas.add(employeeExcelDao);
            }
            workbook.close();

            return datas;

        } catch (Exception e){
            throw new RuntimeException("Failed: " + e.getMessage());
        }
    }

    private Boolean checkExcel(MultipartFile file){
        if(TYPE.equals(file.getContentType())){
            return true;
        } else {
            return false;
        }
    }

    private CheckExcelDao checkAllExcelData (List<EmployeeExcelDao> excelList, String username){
        Integer rowErrorCount = 0;
        LocalDateTime now = LocalDateTime.now();
        List<Employee> employees = new ArrayList<Employee>();
        List<History> histories = new ArrayList<History>();
        Employee emp = new Employee();
        Employee tmp = new Employee();
        History hst = new History();
        for (int i = 0; i < excelList.size(); i++){
            if (Objects.isNull(excelList.get(i).getEmployeeid())){
                Employee employee = new Employee();
                if(!Objects.isNull(employeeService.getByCallsign(excelList.get(i).getCallsign()))){
                    rowErrorCount++;
                    continue;
                } else {
                    employee.setCallsign(excelList.get(i).getCallsign());
                }

                if(!Objects.isNull(employeeService.getByNikKtp(excelList.get(i).getNikktp()))){
                    rowErrorCount++;
                    continue;
                } else {
                    employee.setNikktp(excelList.get(i).getNikktp());
                }

                if(!Objects.isNull(employeeService.getByNIK(excelList.get(i).getNik()))){
                    rowErrorCount++;
                    continue;
                } else {
                    employee.setNik(excelList.get(i).getNik());
                }

                if(excelList.get(i).getStatus().getStatus().equals("ACTIVE")){
                    employee.setStatus(excelList.get(i).getStatus());
                } else {
                    rowErrorCount++;
                    continue;
                }

                String joinCallsign = employeeService.getDDMMYYYY();
                String historyCode = employeeService.fixedHistoryCode();
                employee.setFullname(excelList.get(i).getFullname());
                employee.setCompany(excelList.get(i).getCompany());
                employee.setRegional(excelList.get(i).getRegional());
                employee.setDivision(excelList.get(i).getDivision());
                employee.setMainJob(excelList.get(i).getMainJob());
                employee.setPosition(excelList.get(i).getPosition());
                employee.setLevel(excelList.get(i).getLevel());
                employee.setEducation(excelList.get(i).getEducation());
                employee.setJoindate(excelList.get(i).getJoindate());
                employee.setJoincallsign(joinCallsign);
                employee.setAddress(excelList.get(i).getAddress());
                employee.setEmail(excelList.get(i).getEmail());
                employee.setPhonenum(excelList.get(i).getPhonenum());
                employee.setRemark(excelList.get(i).getRemark());
                employee.setCreatedby(username);
                employee.setDatecreated(now);
                employee.setHistorycode(historyCode);

                //history add
                History newHistory = new History();
                newHistory.setType("NEW");
                newHistory.setCreateddate(now);
                newHistory.setJoincallsign(joinCallsign);
                newHistory.setCreatedby(username);
                newHistory.setHistorycode(historyCode);
                //end history add

                histories.add(newHistory);
                employees.add(employee);
            } else {
                Employee employee = employeeService.getEmpActiveById(excelList.get(i).getEmployeeid());
                History history = new History();
                String historyOrNot = "NO";
                //record history
                history.setEmployeeidfrom(employee.getEmployeeid());
                history.setHistorycode(employee.getHistorycode());
                //end record history
                if(Objects.isNull(employee)){
                    rowErrorCount++;
                    continue;
                } else {
                    Employee tempEmployee = employeeService.getEmpActiveById(excelList.get(i).getEmployeeid());
                    if (employee.getCallsign().equals(excelList.get(i).getCallsign())){
                        employee.setCallsign(excelList.get(i).getCallsign());
                        historyOrNot = "NO";
                    } else {
                        if(!Objects.isNull(employeeService.getByCallsign(excelList.get(i).getCallsign()))){
                            rowErrorCount++;
                            continue;
                        } else {
                            //history record callsign
                            historyOrNot = "YES";
                            history.setCallsignfrom(employee.getCallsign());
                            history.setCallsignto(excelList.get(i).getCallsign());
                            history.setJoincallsign(employee.getJoincallsign());
                            //end history record callsign
                            employee.setEmployeeid(null);
                            employee.setCallsign(excelList.get(i).getCallsign());
                            employee.setReleasecallsign(employeeService.getDDMMYYYY());

                        }
                    }

                    if (employee.getNikktp().equals(excelList.get(i).getNikktp())) {
                        employee.setNikktp(excelList.get(i).getNikktp());
                    } else {
                        if(!Objects.isNull(employeeService.getByNikKtp(excelList.get(i).getNikktp()))){
                            rowErrorCount++;
                            continue;
                        } else {
                            employee.setNikktp(excelList.get(i).getNikktp());
                        }
                    }

                    if(employee.getNik().equals(excelList.get(i).getNik())){
                        employee.setNik(excelList.get(i).getNik());
                    } else {
                        if(!Objects.isNull(employeeService.getByNIK(excelList.get(i).getNik()))){
                            rowErrorCount++;
                            continue;
                        } else {
                            employee.setNik(excelList.get(i).getNik());
                        }
                    }

                    employee.setFullname(excelList.get(i).getFullname());

                    if (employee.getCompany().equals(excelList.get(i).getCompany())){
                        employee.setCompany(excelList.get(i).getCompany());
                        historyOrNot = "NO";
                    } else {
                        //history record company
                        historyOrNot = "YES";
                        history.setCompanyfrom(employee.getCompany().getCompanyname());
                        history.setCompanyto(excelList.get(i).getCompany().getCompanyname());
                        //end history record company
                        employee.setEmployeeid(null);
                        employee.setCompany(excelList.get(i).getCompany());
                    }

                    if (employee.getRegional().equals(excelList.get(i).getRegional())){
                        employee.setRegional(excelList.get(i).getRegional());
                        historyOrNot = "NO";
                    } else {
                        //history record regional
                        historyOrNot = "YES";
                        history.setRegionalfrom(employee.getRegional().getRegionaldept());
                        history.setRegionalto(excelList.get(i).getRegional().getRegionaldept());
                        //end history record regional
                        employee.setEmployeeid(null);
                        employee.setRegional(excelList.get(i).getRegional());
                    }

                    if (employee.getDivision().equals(excelList.get(i).getDivision())){
                        employee.setDivision(excelList.get(i).getDivision());
                        historyOrNot = "NO";
                    } else {
                        //history record division
                        historyOrNot = "YES";
                        history.setDivisionfrom(employee.getDivision().getDivisioname());
                        history.setDivisionto(excelList.get(i).getDivision().getDivisioname());
                        //end history record division
                        employee.setEmployeeid(null);
                        employee.setDivision(excelList.get(i).getDivision());
                    }

                    if (employee.getMainJob().equals(excelList.get(i).getMainJob())){
                        employee.setMainJob(excelList.get(i).getMainJob());
                        historyOrNot = "NO";
                    } else {
                        //history record mainjob
                        historyOrNot = "YES";
                        history.setMainjobfrom(employee.getMainJob().getMainjobname());
                        history.setMainjobto(excelList.get(i).getMainJob().getMainjobname());
                        //end history record mainjob
                        employee.setEmployeeid(null);
                        employee.setMainJob(excelList.get(i).getMainJob());
                    }

                    if (employee.getPosition().equals(excelList.get(i).getPosition())){
                        employee.setPosition(excelList.get(i).getPosition());
                        historyOrNot = "NO";
                    } else {
                        //history record position
                        historyOrNot = "YES";
                        history.setPositionfrom(employee.getPosition().getPositionname());
                        history.setPositionto(excelList.get(i).getPosition().getPositionname());
                        //end history record position
                        employee.setEmployeeid(null);
                        employee.setPosition(excelList.get(i).getPosition());
                    }

                    if (employee.getLevel().equals(excelList.get(i).getLevel())){
                        employee.setLevel(excelList.get(i).getLevel());
                        historyOrNot = "NO";
                    } else {
                        //history record level
                        historyOrNot = "YES";
                        history.setLevelfrom(employee.getLevel().getLevelname());
                        history.setLevelto(excelList.get(i).getLevel().getLevelname());
                        //end history record level
                        employee.setEmployeeid(null);
                        employee.setLevel(excelList.get(i).getLevel());
                    }

                    employee.setEducation(excelList.get(i).getEducation());
                    employee.setAddress(excelList.get(i).getAddress());
                    employee.setEmail(excelList.get(i).getEmail());
                    employee.setPhonenum(excelList.get(i).getPhonenum());
                    employee.setRemark(excelList.get(i).getRemark());
                    employee.setRemark_promote(excelList.get(i).getRemark_promote());
                    employee.setRemark_movement(excelList.get(i).getRemark_movement());
                    employee.setModifiedby(username);
                    employee.setModifiedon(now);
                    //record history
                    history.setType("UPDATE");
                    history.setHistoryfromto("EXCEL TEMPLATE");
                    history.setCreatedby(username);
                    history.setCreateddate(now);
                    //end record history
                    if(excelList.get(i).getStatus().getStatus().equals("INACTIVE")){
                        employee.setStatus(excelList.get(i).getStatus());
                        employee.setRemark_inactive(excelList.get(i).getRemark_inactive());
                    } else if (excelList.get(i).getStatus().getStatus().equals("RESIGN")){
                        employee.setStatus(excelList.get(i).getStatus());
                        employee.setRemark_resign(excelList.get(i).getRemark_resign());
                        employee.setReleasecallsign(employeeService.getDDMMYYYY());
                        employee.setResigndate(excelList.get(i).getResigndate());
                    } else if (excelList.get(i).getStatus().getStatus().equals("OFF")) {
                        rowErrorCount++;
                        continue;
                    } else {
                        if (Objects.isNull(employee.getEmployeeid())){
                            Status status = statusService.getByStatus("INACTIVE");
                            tempEmployee.setStatus(status);
                        } else {
                            employee.setStatus(excelList.get(i).getStatus());
                        }
                    }

                    if(Objects.isNull(employee.getEmployeeid())){
                        employees.add(employee);
                        employees.add(tempEmployee);
                        histories.add(history);
                    } else {
                        employees.add(employee);
                        if(historyOrNot.equals("YES")){
                            histories.add(history);
                        }
                    }
                }
            }
        }

        CheckExcelDao response = new CheckExcelDao();
        response.setErrorCount(rowErrorCount);
        response.setEmployees(employees);
        response.setHistories(histories);
        return response;
    }

    private CheckExcelDao saveAllData (List<EmployeeExcelDao> excelList, String username){
        Integer rowErrorCount = 0;
        LocalDateTime now = LocalDateTime.now();
        List<Employee> employees = new ArrayList<Employee>();
        List<History> histories = new ArrayList<History>();
        Employee emp = new Employee();
        Employee tmp = new Employee();
        History hst = new History();
        for (int i = 0; i < excelList.size(); i++){
            if (Objects.isNull(excelList.get(i).getEmployeeid())){
                Employee employee = new Employee();
                if(!Objects.isNull(employeeService.getByCallsign(excelList.get(i).getCallsign()))){
                    rowErrorCount++;
                    continue;
                } else {
                    employee.setCallsign(excelList.get(i).getCallsign());
                }

                if(!Objects.isNull(employeeService.getByNikKtp(excelList.get(i).getNikktp()))){
                    rowErrorCount++;
                    continue;
                } else {
                    employee.setNikktp(excelList.get(i).getNikktp());
                }

                if(!Objects.isNull(employeeService.getByNIK(excelList.get(i).getNik()))){
                    rowErrorCount++;
                    continue;
                } else {
                    employee.setNik(excelList.get(i).getNik());
                }

                if(excelList.get(i).getStatus().getStatus().equals("ACTIVE")){
                    employee.setStatus(excelList.get(i).getStatus());
                } else {
                    rowErrorCount++;
                    continue;
                }

                String joinCallsign = employeeService.getDDMMYYYY();
                String historyCode = employeeService.fixedHistoryCode();
                employee.setFullname(excelList.get(i).getFullname());
                employee.setCompany(excelList.get(i).getCompany());
                employee.setRegional(excelList.get(i).getRegional());
                employee.setDivision(excelList.get(i).getDivision());
                employee.setMainJob(excelList.get(i).getMainJob());
                employee.setPosition(excelList.get(i).getPosition());
                employee.setLevel(excelList.get(i).getLevel());
                employee.setEducation(excelList.get(i).getEducation());
                employee.setJoindate(excelList.get(i).getJoindate());
                employee.setJoincallsign(joinCallsign);
                employee.setAddress(excelList.get(i).getAddress());
                employee.setEmail(excelList.get(i).getEmail());
                employee.setPhonenum(excelList.get(i).getPhonenum());
                employee.setRemark(excelList.get(i).getRemark());
                employee.setCreatedby(username);
                employee.setDatecreated(now);
                employee.setHistorycode(historyCode);

                //history add
                History newHistory = new History();
                newHistory.setType("NEW");
                newHistory.setCreateddate(now);
                newHistory.setJoincallsign(joinCallsign);
                newHistory.setCreatedby(username);
                newHistory.setHistorycode(historyCode);
                //end history add


                Employee employeeSaved = employeeRepository.saveAndFlush(employee);
                employees.add(employeeSaved);
                History historySaved = historyRepository.saveAndFlush(newHistory);
                histories.add(historySaved);
            } else {
                String historyOrNot = "NO";
                Employee employee = employeeService.getEmpActiveById(excelList.get(i).getEmployeeid());
                History history = new History();
                //record history
                history.setEmployeeidfrom(employee.getEmployeeid());
                history.setHistorycode(employee.getHistorycode());
                //end record history
                if(Objects.isNull(employee)){
                    rowErrorCount++;
                    continue;
                } else {
                    Employee tempEmployee = employeeService.getEmpActiveById(excelList.get(i).getEmployeeid());
                    if (employee.getCallsign().equals(excelList.get(i).getCallsign())){
                        employee.setCallsign(excelList.get(i).getCallsign());
                        historyOrNot = "NO";
                    } else {
                        if(!Objects.isNull(employeeService.getByCallsign(excelList.get(i).getCallsign()))){
                            rowErrorCount++;
                            continue;
                        } else {
                            //history record callsign
                            historyOrNot = "YES";
                            history.setCallsignfrom(employee.getCallsign());
                            history.setCallsignto(excelList.get(i).getCallsign());
                            history.setJoincallsign(employee.getJoincallsign());
                            //end history record callsign
                            employee.setEmployeeid(null);
                            employee.setCallsign(excelList.get(i).getCallsign());
                            employee.setReleasecallsign(employeeService.getDDMMYYYY());
                        }
                    }

                    if (employee.getNikktp().equals(excelList.get(i).getNikktp())) {
                        employee.setNikktp(excelList.get(i).getNikktp());
                    } else {
                        if(!Objects.isNull(employeeService.getByNikKtp(excelList.get(i).getNikktp()))){
                            rowErrorCount++;
                            continue;
                        } else {
                            employee.setNikktp(excelList.get(i).getNikktp());
                        }
                    }

                    if(employee.getNik().equals(excelList.get(i).getNik())){
                        employee.setNik(excelList.get(i).getNik());
                    } else {
                        if(!Objects.isNull(employeeService.getByNIK(excelList.get(i).getNik()))){
                            rowErrorCount++;
                            continue;
                        } else {
                            employee.setNik(excelList.get(i).getNik());
                        }
                    }

                    employee.setFullname(excelList.get(i).getFullname());

                    if (employee.getCompany().equals(excelList.get(i).getCompany())){
                        employee.setCompany(excelList.get(i).getCompany());
                        historyOrNot = "NO";
                    } else {
                        //history record company
                        historyOrNot = "YES";
                        history.setCompanyfrom(employee.getCompany().getCompanyname());
                        history.setCompanyto(excelList.get(i).getCompany().getCompanyname());
                        //end history record company
                        employee.setEmployeeid(null);
                        employee.setCompany(excelList.get(i).getCompany());
                    }

                    if (employee.getRegional().equals(excelList.get(i).getRegional())){
                        employee.setRegional(excelList.get(i).getRegional());
                        historyOrNot = "NO";
                    } else {
                        //history record regional
                        historyOrNot = "YES";
                        history.setRegionalfrom(employee.getRegional().getRegionaldept());
                        history.setRegionalto(excelList.get(i).getRegional().getRegionaldept());
                        //end history record regional
                        employee.setEmployeeid(null);
                        employee.setRegional(excelList.get(i).getRegional());
                    }

                    if (employee.getDivision().equals(excelList.get(i).getDivision())){
                        employee.setDivision(excelList.get(i).getDivision());
                        historyOrNot = "NO";
                    } else {
                        //history record division
                        historyOrNot = "YES";
                        history.setDivisionfrom(employee.getDivision().getDivisioname());
                        history.setDivisionto(excelList.get(i).getDivision().getDivisioname());
                        //end history record division
                        employee.setEmployeeid(null);
                        employee.setDivision(excelList.get(i).getDivision());
                    }

                    if (employee.getMainJob().equals(excelList.get(i).getMainJob())){
                        employee.setMainJob(excelList.get(i).getMainJob());
                        historyOrNot = "NO";
                    } else {
                        //history record mainjob
                        historyOrNot = "YES";
                        history.setMainjobfrom(employee.getMainJob().getMainjobname());
                        history.setMainjobto(excelList.get(i).getMainJob().getMainjobname());
                        //end history record mainjob
                        employee.setEmployeeid(null);
                        employee.setMainJob(excelList.get(i).getMainJob());
                    }

                    if (employee.getPosition().equals(excelList.get(i).getPosition())){
                        employee.setPosition(excelList.get(i).getPosition());
                        historyOrNot = "NO";
                    } else {
                        //history record position
                        historyOrNot = "YES";
                        history.setPositionfrom(employee.getPosition().getPositionname());
                        history.setPositionto(excelList.get(i).getPosition().getPositionname());
                        //end history record position
                        employee.setEmployeeid(null);
                        employee.setPosition(excelList.get(i).getPosition());
                    }

                    if (employee.getLevel().equals(excelList.get(i).getLevel())){
                        employee.setLevel(excelList.get(i).getLevel());
                        historyOrNot = "NO";
                    } else {
                        //history record level
                        historyOrNot = "YES";
                        history.setLevelfrom(employee.getLevel().getLevelname());
                        history.setLevelto(excelList.get(i).getLevel().getLevelname());
                        //end history record level
                        employee.setEmployeeid(null);
                        employee.setLevel(excelList.get(i).getLevel());
                    }

                    employee.setEducation(excelList.get(i).getEducation());
                    employee.setAddress(excelList.get(i).getAddress());
                    employee.setEmail(excelList.get(i).getEmail());
                    employee.setPhonenum(excelList.get(i).getPhonenum());
                    employee.setRemark(excelList.get(i).getRemark());
                    employee.setRemark_promote(excelList.get(i).getRemark_promote());
                    employee.setRemark_movement(excelList.get(i).getRemark_movement());
                    employee.setModifiedby(username);
                    employee.setModifiedon(now);
                    //record history
                    history.setType("UPDATE");
                    history.setHistoryfromto("EXCEL TEMPLATE");
                    history.setCreatedby(username);
                    history.setCreateddate(now);
                    //end record history
                    if(excelList.get(i).getStatus().getStatus().equals("INACTIVE")){
                        employee.setStatus(excelList.get(i).getStatus());
                        employee.setRemark_inactive(excelList.get(i).getRemark_inactive());
                    } else if (excelList.get(i).getStatus().getStatus().equals("RESIGN")){
                        employee.setStatus(excelList.get(i).getStatus());
                        employee.setRemark_resign(excelList.get(i).getRemark_resign());
                        employee.setReleasecallsign(employeeService.getDDMMYYYY());
                        if (Objects.isNull(excelList.get(i).getResigndate())){
                            employee.setResigndate(employeeService.getDDMMYYYY());
                        } else {
                            employee.setResigndate(excelList.get(i).getResigndate());
                        }
                    } else if (excelList.get(i).getStatus().getStatus().equals("OFF")) {
                        employee.setStatus(excelList.get(i).getStatus());
                    } else {
                        if (Objects.isNull(employee.getEmployeeid())){
                            Status status = statusService.getByStatus("INACTIVE");
                            tempEmployee.setStatus(status);
                        } else {
                            employee.setStatus(excelList.get(i).getStatus());
                        }
                    }

                    if(Objects.isNull(employee.getEmployeeid())){
                        Employee employeeSaved = employeeRepository.saveAndFlush(employee);
                        employees.add(employeeSaved);
                        Employee temEmployeeSaved = employeeRepository.saveAndFlush(tempEmployee);
                        History tempHistory = history;
                        tempHistory.setEmployeeidto(employeeSaved.getEmployeeid());
                        History historySaved = historyRepository.saveAndFlush(tempHistory);
                        histories.add(historySaved);
                    } else {
                        Employee employeeSaved = employeeRepository.saveAndFlush(employee);
                        employees.add(employeeSaved);
                        if (historyOrNot.equals("YES")){
                            History tempHistory = history;
                            tempHistory.setEmployeeidto(employeeSaved.getEmployeeid());
                            History historySaved = historyRepository.saveAndFlush(tempHistory);
                            histories.add(historySaved);
                        }
                    }
                }
            }
        }

        CheckExcelDao response = new CheckExcelDao();
        response.setErrorCount(rowErrorCount);
        response.setEmployees(employees);
        response.setHistories(histories);
        return response;
    }

    public void getExcelReportEmployee(HttpServletRequest request, HttpServletResponse response, String company, String regional, String division, String position){
        response.setContentType("application/msexcel");
        response.setHeader("Content-Disposition", "attachment; filename=\"TestDown.xlsx\"");
        try {
            InputStream io = new ClassPathResource("templates/employeeReport.xlsx").getInputStream();
            OutputStream os = response.getOutputStream();

            //list for employees data
            List<EmployeeReportDao> employees = employeeService.getDataForReportEmployee(company, regional, division, position);
            //end list for employees data

            Context context = new Context();

            //set params
            context.putVar("EmployeeList", employees);
            //end set params

            JxlsHelper.getInstance().processTemplate(io, os, context);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getExcelReportResignEmployee(HttpServletRequest request, HttpServletResponse response, String startdate, String enddate, String company){
        response.setContentType("application/msexcel");
        response.setHeader("Content-Disposition", "attachment; filename=\"TestDown.xlsx\"");
        try {
            InputStream io = new ClassPathResource("templates/employeeResignReport.xlsx").getInputStream();
            OutputStream os = response.getOutputStream();

            //list for employees data
            List<EmployeeReportDao> employees = employeeService.getDataForReportResignEmployee(startdate, enddate, company);
            //end list for employees data

            Context context = new Context();

            //set params
            context.putVar("EmployeeList", employees);
            //end set params

            JxlsHelper.getInstance().processTemplate(io, os, context);

        } catch (Exception e){
            e.printStackTrace();
        }
    }



}
