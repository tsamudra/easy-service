package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.MainJob;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface MainjobRepository extends JpaRepository<MainJob, Integer> {

    MainJob getByMainjobid(Integer id);
    MainJob getByMainjobname(String mainjobname);
    @Transactional
    Integer removeByMainjobid(Integer id);
}
