package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.LevelListDao;
import io.easy.easyservice.models.entity.LevelList;
import io.easy.easyservice.services.LevelListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class LevelListController {
    @Autowired
    private LevelListService levelListService;

    @GetMapping("/getalllevellist")
    public List<LevelList> getAll(){
        return levelListService.getAll();
    }

    @GetMapping("/getlevellistbyid")
    public LevelList getById(@RequestParam Integer id){
        return levelListService.getById(id);
    }

    @GetMapping("/getlevellistbyrecord")
    public LevelList getByRecord(@RequestParam Integer poisitionid, @RequestParam Integer levelid){
        return levelListService.getByRecord(poisitionid, levelid);
    }

    @PostMapping("/createlevellist")
    public LevelList createLevelList(@RequestBody LevelListDao levelListDao){
        return levelListService.createLevelList(levelListDao);
    }

    @PatchMapping("/updatelevellist")
    public LevelList updateLevelList(@RequestBody LevelListDao levelListDao){
        return levelListService.updateLevelList(levelListDao);
    }

    @DeleteMapping("/deletelevellist")
    public Integer deleteLevelList(@RequestParam Integer id){
        return levelListService.deleteLevelList(id);
    }
}
