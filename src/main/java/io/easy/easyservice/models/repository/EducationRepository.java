package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Education;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface EducationRepository extends JpaRepository<Education, Integer> {

    Education findByEduid(Integer id);
    Education findByEducationname(String eduname);
    @Transactional
    Integer removeByEduid(Integer id);

}
