package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.EducationDao;
import io.easy.easyservice.models.entity.Education;
import io.easy.easyservice.models.repository.EducationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class EducationService {

    @Autowired
    private EducationRepository educationRepository;

    public List<Education> getAllEdu(){
        return educationRepository.findAll();
    }

    public Education getEdubyId (Integer id){
        return educationRepository.findByEduid(id);
    }

    public Education getEdubyName (String eduname){
        return educationRepository.findByEducationname(eduname);
    }

    public Education createNewEducation (EducationDao educationDao){
        Education checkAvailability = getEdubyName(educationDao.getEducationname());
        if(Objects.isNull(checkAvailability)){
            LocalDateTime now = LocalDateTime.now();
            Education tempEdu = new Education();
            tempEdu.setEduid(null);
            tempEdu.setEducationname(educationDao.getEducationname());
            tempEdu.setDatecreated(now);
            return educationRepository.saveAndFlush(tempEdu);
        } else {
            Education response = new Education();
            response.setEducationname("Education Name Used, Cannot Save Duplicate Data");
            return response
                    ;
        }
    }

    public Education updateEducation (EducationDao educationDao){
        Education eduFromDB = getEdubyId(educationDao.getEduid());
        Education checkEdu = getEdubyName(educationDao.getEducationname());
        if (!Objects.isNull(eduFromDB)) {
            if (eduFromDB.getEducationname().equals(educationDao.getEducationname())){
                eduFromDB.setEducationname(educationDao.getEducationname());
                return educationRepository.saveAndFlush(eduFromDB);
            } else {
                if (Objects.isNull(checkEdu)){
                    eduFromDB.setEducationname(educationDao.getEducationname());
                    return educationRepository.saveAndFlush(eduFromDB);
                } else {
                    Education response = new Education();
                    response.setEducationname("Education Name Used, Cannot Save Duplicate Data");
                    return response;
                }
            }
        } else {
            return new Education();
        }
    }

    public Integer deleteEducation (Integer id) {
        return educationRepository.removeByEduid(id);
    }

}
