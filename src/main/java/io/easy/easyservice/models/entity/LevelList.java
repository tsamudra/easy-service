package io.easy.easyservice.models.entity;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "level_list")
@Data
public class LevelList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer levellistid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "positionid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Position position;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "levelid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Level level;

    private LocalDateTime datecreated;

}
