package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.PositionDao;
import io.easy.easyservice.models.entity.Position;
import io.easy.easyservice.services.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class PositionController {

    @Autowired
    private PositionService positionService;

    @GetMapping("/getallposition")
    public List<Position> getAll(){
        return positionService.getAll();
    }

    @GetMapping("/getpositionbyid")
    public Position getById(@RequestParam Integer id){
        return positionService.getById(id);
    }

    @GetMapping("/getpositionbyname")
    public Position getByName(@RequestParam String postname){
        return positionService.getByName(postname);
    }

    @PostMapping("/createposition")
    public Position createPosition(@RequestBody PositionDao positionDao){
        return positionService.createPosition(positionDao);
    }

    @PatchMapping("/updateposition")
    public Position updatePosition(@RequestBody PositionDao positionDao){
        return positionService.updatePosition(positionDao);
    }

    @DeleteMapping("/deleteposition")
    public Integer deletePosition(@RequestParam Integer id){
        return positionService.deletePosition(id);
    }
}
