package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Division;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface DivisionRepository extends JpaRepository<Division, Integer> {

    Division findByDivisionid(Integer id);
    Division findByDivisioname(String hubname);
    @Transactional
    Integer removeByDivisionid(Integer id);
}
