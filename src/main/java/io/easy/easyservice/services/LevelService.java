package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.LevelDao;
import io.easy.easyservice.models.entity.Level;
import io.easy.easyservice.models.repository.LevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class LevelService {

    @Autowired
    private LevelRepository levelRepository;

    public List<Level> getAll(){
        return levelRepository.findAll();
    }

    public Level getById(Integer id){
        return levelRepository.findByLevelid(id);
    }

    public Level getByName(String levelname){
        return levelRepository.findByLevelname(levelname);
    }

    public Level creteLevel(LevelDao levelDao){
        Level checkAvailability = getByName(levelDao.getLevelname());
        if(Objects.isNull(checkAvailability)){
            LocalDateTime now = LocalDateTime.now();
            Level tempLevel = new Level();
            tempLevel.setLevelid(null);
            tempLevel.setLevelname(levelDao.getLevelname());
            tempLevel.setDatecreated(now);
            return levelRepository.saveAndFlush(tempLevel);
        } else {
            Level response = new Level();
            response.setLevelname("Level Name Used. Cannot Save Duplicate Data");
            return response;
        }
    }

    public Level updateLevel(LevelDao levelDao){
        Level levelFromDB = getById(levelDao.getLevelid());
        Level checkLevel = getByName(levelDao.getLevelname());
        if (!Objects.isNull(levelFromDB)){
            if(levelFromDB.getLevelname().equals(levelDao.getLevelname())){
                levelFromDB.setLevelname(levelDao.getLevelname());
                return levelRepository.saveAndFlush(levelFromDB);
            } else {
                if (Objects.isNull(checkLevel)){
                    levelFromDB.setLevelname(levelDao.getLevelname());
                    return levelRepository.saveAndFlush(levelFromDB);
                } else {
                    Level response = new Level();
                    response.setLevelname("Level Name Used. Cannot Save Duplicate Data");
                    return new Level();
                }
            }

        } else {
            return new Level();
        }
    }

    public Integer deleteLevel(Integer id){
        return levelRepository.removeByLevelid(id);
    }
}
