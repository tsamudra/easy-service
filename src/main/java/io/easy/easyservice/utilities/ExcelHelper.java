package io.easy.easyservice.utilities;

import io.easy.easyservice.models.dao.EmployeeExcelDao;
import io.easy.easyservice.models.entity.*;
import io.easy.easyservice.services.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelHelper {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private RegionalService regionalService;

    @Autowired
    private DivisionService divisionService;

    @Autowired
    private MainjobService mainjobService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private EducationService educationService;

    @Autowired
    private StatusService statusService;

    public static  String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String[] HEADERs = {"employeeid", "address", "callsign", "fullname", "joindate", "nik", "phonenum", "resigndate", "companyid", "educationid", "mainjobid", "positionid", "regionalid", "statusid", "datecreated", "email", "joincallsign", "releasecallsign", "remark" , "remark_movement", "remark_promote", "remark_resign", "levelid", "nikktp", "createdby", "divisionid", "historycode", "remark_inactive", "modifiedby", "modifiedon"};
    static String SHEET = "template";

    public Boolean checkExcel(MultipartFile file){
        if(!TYPE.equals(file.getContentType())){
            return false;
        } else {
            return true;
        }
    }

    public List<EmployeeExcelDao> excelToList(InputStream is) {
        try {
            Workbook workbook = new XSSFWorkbook();
            Sheet sheet =workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<EmployeeExcelDao> datas = new ArrayList<EmployeeExcelDao>();
            Integer i = 0;

            while (rows.hasNext()){
                Row currentRow = rows.next();

                //headers
                if ( (i.equals(0)) || (i.equals(1))){
                    i++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                EmployeeExcelDao employeeExcelDao = new EmployeeExcelDao();

                Integer j = 0;

                while (cellsInRow.hasNext()){
                    Cell currentCell = cellsInRow.next();
                    switch (j){
                        case 0:
                            employeeExcelDao.setEmployeeid(Integer.parseInt(currentCell.getStringCellValue()));
                            break;

                        case 1:
                            employeeExcelDao.setCallsign(currentCell.getStringCellValue());
                            break;

                        case 2:
                            employeeExcelDao.setNikktp(currentCell.getStringCellValue());
                            break;

                        case 3:
                            employeeExcelDao.setNik(currentCell.getStringCellValue());
                            break;

                        case 4:
                            employeeExcelDao.setFullname(currentCell.getStringCellValue());
                            break;

                        case 5:
                            Company company = companyService.getCompanybyName(currentCell.getStringCellValue());
                            employeeExcelDao.setCompany(company);
                            break;

                        case 6:
                            Regional regional = regionalService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setRegional(regional);
                            break;

                        case 7:
                            Division division = divisionService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setDivision(division);
                            break;

                        case 8:
                            MainJob mainJob = mainjobService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setMainJob(mainJob);
                            break;

                        case 9:
                            Position position = positionService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setPosition(position);
                            break;

                        case 10:
                            Level level = levelService.getByName(currentCell.getStringCellValue());
                            employeeExcelDao.setLevel(level);
                            break;

                        case 11:
                            employeeExcelDao.setEmail(currentCell.getStringCellValue());
                            break;

                        case 12:
                            employeeExcelDao.setAddress(currentCell.getStringCellValue());
                            break;

                        case 13:
                            Education education = educationService.getEdubyName(currentCell.getStringCellValue());
                            employeeExcelDao.setEducation(education);
                            break;

                        case 14:
                            employeeExcelDao.setPhonenum(currentCell.getStringCellValue());
                            break;

                        case 15:
                            employeeExcelDao.setJoindate(currentCell.getStringCellValue());
                            break;

                        case 16:
                            employeeExcelDao.setRemark(currentCell.getStringCellValue());
                            break;

                        case 17:
                            employeeExcelDao.setRemark_promote(currentCell.getStringCellValue());
                            break;

                        case 18:
                            employeeExcelDao.setRemark_movement(currentCell.getStringCellValue());
                            break;

                        case 19:
                            employeeExcelDao.setRemark_inactive(currentCell.getStringCellValue());
                            break;

                        case 20:
                            Status status = statusService.getByStatus(currentCell.getStringCellValue());
                            employeeExcelDao.setStatus(status);
                            break;

                        default:
                            break;
                    }
                    j++;
                }
                datas.add(employeeExcelDao);
            }
            workbook.close();

            return datas;

        } catch (Exception e){
            throw new RuntimeException("Failed: " + e.getMessage());
        }
    }
}
