package io.easy.easyservice.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DownloadParamsDao {
    private String company;
    private String regional;
    private String division;
    private String position;
    private String startdate;
    private String enddate;
}
