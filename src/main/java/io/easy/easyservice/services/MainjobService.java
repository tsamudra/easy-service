package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.MainjobDao;
import io.easy.easyservice.models.entity.MainJob;
import io.easy.easyservice.models.repository.MainjobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class MainjobService {

    @Autowired
    private MainjobRepository mainjobRepository;

    public List<MainJob> getAllMainjob(){
        return mainjobRepository.findAll();
    }

    public MainJob getByid (Integer id) {
        return mainjobRepository.getByMainjobid(id);
    }

    public MainJob getByName (String mainjobname){
        return mainjobRepository.getByMainjobname(mainjobname);
    }

    public MainJob createNewMainjob (MainjobDao mainjobDao) {
        MainJob checkAvailabillity = getByName(mainjobDao.getMainjobname());
        if(Objects.isNull(checkAvailabillity)){
            LocalDateTime now = LocalDateTime.now();
            MainJob tempMainjob = new MainJob();
            tempMainjob.setMainjobid(null);
            tempMainjob.setMainjobname(mainjobDao.getMainjobname());
            tempMainjob.setDatecreated(now);
            return mainjobRepository.saveAndFlush(tempMainjob);
        } else {
            MainJob response = new MainJob();
            response.setMainjobname("Main Job Name Used, Cannot Save Duplicate Data");
            return response;
        }
    }

    public MainJob updateMainjob (MainjobDao mainjobDao){
        MainJob mainJobFromDB = getByid(mainjobDao.getMainjobid());
        MainJob checkMainjob = getByName(mainjobDao.getMainjobname());
        if (!Objects.isNull(mainJobFromDB)){
            if (mainJobFromDB.getMainjobname().equals(mainjobDao.getMainjobname())){
                mainJobFromDB.setMainjobname(mainjobDao.getMainjobname());
                return mainjobRepository.saveAndFlush(mainJobFromDB);
            } else {
                if (Objects.isNull(checkMainjob)){
                    mainJobFromDB.setMainjobname(mainjobDao.getMainjobname());
                    return mainjobRepository.saveAndFlush(mainJobFromDB);
                } else {
                    MainJob response = new MainJob();
                    response.setMainjobname("Main Job Name Used, Cannot Save Duplicate Data");
                    return response;
                }
            }
        } else {
            return new MainJob();
        }
    }

    public Integer deleteMainjob (Integer id) {
        return mainjobRepository.removeByMainjobid(id);
    }

}
