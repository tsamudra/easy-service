package io.easy.easyservice.models.dao;

import io.easy.easyservice.models.entity.Employee;
import io.easy.easyservice.models.entity.History;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckExcelDao {
    private Integer errorCount;
    private List<Employee> employees;
    private List<History> histories;
}
