package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.BulkDao;
import io.easy.easyservice.models.dao.EmployeeDao;
import io.easy.easyservice.models.entity.Employee;
import io.easy.easyservice.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/getallactiveemployee")
    public List<Employee> getAllActive(){
        return employeeService.getAllActive();
    }

    @GetMapping("/getallactiveemployeewithfilter")
    public List<Employee> getAllActiveEmployeeWithFilter(@RequestParam("company") String company, @RequestParam("regional") String regional, @RequestParam("division") String division, @RequestParam("position") String position){
        return employeeService.getAllActiveWithFilter(company, regional, division, position);
    }

    @GetMapping("/getallresignemployeewithfilter")
    public List<Employee> getAllResignEmployeeWithFilter(@RequestParam("startdate") String startdate, @RequestParam("enddate") String enddate, @RequestParam("company") String company){
        return employeeService.getAllResignWithFilter(startdate, enddate, company);
    }

    @GetMapping("/getalloffemployeewithfilter")
    public List<Employee> getAllOffEmployeeWithFilter(@RequestParam("startdate") String startdate, @RequestParam("enddate") String enddate){
        return employeeService.getAllOffWithFilter(startdate, enddate);
    }

    @GetMapping("/getalloffemployee")
    public List<Employee> getAllInactive(){
        return employeeService.getAllOff();
    }

    @GetMapping("/getallresignemployee")
    public List<Employee> getAllResign(){
        return employeeService.getAllResign();
    }

    @GetMapping("/getemployeebyid")
    public Employee getById(@RequestParam Integer id){
        return employeeService.getEmpActiveById(id);
    }

    @GetMapping("/getempbyid")
    public Employee getEmpById(@RequestParam Integer id){
        return employeeService.getByid(id);
    }

    @GetMapping("/getemployeebycallsign")
    public Employee getByCallsign(@RequestParam String callsign){
        return employeeService.getByCallsign(callsign);
    }

    @GetMapping("/getemployeebynikktp")
    public Employee getByNikKtp(@RequestParam String nikktp){
        return employeeService.getByNikKtp(nikktp);
    }

    @PostMapping("/createnewemployee")
    public Employee createEmployee(@RequestBody EmployeeDao employeeDao){
        return employeeService.createEmployee(employeeDao);
    }

    @PatchMapping("/updateemployeedetails")
    public Employee updateDetailsEmployee(@RequestBody EmployeeDao employeeDao){
        return employeeService.updateEmployeeDetails(employeeDao);
    }

    @PatchMapping("/updateemployeecallsign")
    public Employee updateEmployeeCallsign(@RequestBody EmployeeDao employeeDao){
        return employeeService.updateCallsign(employeeDao);
    }

    @PatchMapping("/updateemployeepromote")
    public Employee updateEmployeePromote(@RequestBody EmployeeDao employeeDao){
        return employeeService.updatePromote(employeeDao);
    }

    @PatchMapping("/updateemployeemovement")
    public Employee updateEmployeeMovement(@RequestBody EmployeeDao employeeDao){
        return employeeService.updateMovement(employeeDao);
    }

    @GetMapping("/softdeleteemployee")
    public Employee inactiveEmployee(@RequestParam("id") Integer id, @RequestParam("username") String username){
        return employeeService.softDeleteEmployee(id, username);
    }

    @PatchMapping("/updateemployeeresign")
    public Employee resignEmployee (@RequestBody EmployeeDao employeeDao){
        return employeeService.employeeResign(employeeDao);
    }

    @PostMapping("/replacementemployee")
    public Employee replacementEmployee(@RequestBody EmployeeDao employeeDao){
        return employeeService.replacementEmployee(employeeDao);
    }

    @PatchMapping("/bulkdeleteemployee")
    public String bulkDelete(@RequestBody BulkDao bulkDao){
        return employeeService.deleteEmployee(bulkDao);
    }

    @PatchMapping("/bulkrestoreemployee")
    public String bulkRestore(@RequestBody BulkDao bulkDao){
        return employeeService.restoreEmployee(bulkDao);
    }

}
