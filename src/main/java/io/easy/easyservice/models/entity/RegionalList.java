package io.easy.easyservice.models.entity;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "regional_list")
@Data
public class RegionalList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer regionallistid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "regionalid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Regional regional;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "divisionid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Division division;

    private LocalDateTime datecreated;
}
