package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
    User findByUserid(Integer id);
    @Transactional
    Integer deleteUserByUserid(Integer id);

}
