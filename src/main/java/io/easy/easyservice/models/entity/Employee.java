package io.easy.easyservice.models.entity;

import lombok.Data;
import lombok.Generated;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "employee")
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer employeeid;
    private String callsign;
    private String nikktp;
    private String nik;
    private String fullname;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "companyid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "regionalid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Regional regional;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "divisionid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Division division;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mainjobid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private MainJob mainJob;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "positionid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Position position;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "levelid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Level level;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "educationid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Education education;


    private String joindate;
    private String resigndate;
    private String joincallsign;
    private String releasecallsign;
    @Column(length = 600)
    private String address;
    private String email;
    private String phonenum;
    @Column(length = 600)
    private String remark;
    @Column(length = 600)
    private String remark_resign;
    @Column(length = 600)
    private String remark_promote;
    @Column(length = 600)
    private String remark_movement;
    @Column(length = 600)
    private String remark_inactive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "statusid", insertable = true, updatable = true, nullable = false)
    @Fetch(FetchMode.JOIN)
    private Status status;

    private LocalDateTime datecreated;
    private LocalDateTime modifiedon;
    private String createdby;
    private String modifiedby;
    private String historycode;
    private Integer replacementemployeeid;








}
