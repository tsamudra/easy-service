package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.LevelListDao;
import io.easy.easyservice.models.entity.Level;
import io.easy.easyservice.models.entity.LevelList;
import io.easy.easyservice.models.entity.Position;
import io.easy.easyservice.models.repository.LevelListRepository;
import io.easy.easyservice.models.repository.LevelRepository;
import io.easy.easyservice.models.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class LevelListService {
    @Autowired
    private LevelListRepository levelListRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private PositionRepository positionRepository;

    public List<LevelList> getAll(){
        return levelListRepository.findAll();
    }

    public LevelList getById(Integer id){
        return levelListRepository.findByLevellistid(id);
    }

    public LevelList getByRecord(Integer positionid, Integer levelid){
        Position position = positionRepository.getByPositionid(positionid);
        Level level = levelRepository.findByLevelid(levelid);
        return levelListRepository.findByPositionAndLevel(position, level);
    }

    public LevelList createLevelList(LevelListDao levelListDao){
        LevelList checkAvailability = getByRecord(levelListDao.getPositionid(), levelListDao.getLevelid());
        if (Objects.isNull(checkAvailability)){
            LocalDateTime now = LocalDateTime.now();
            LevelList tempLevelList = new LevelList();
            Position position = positionRepository.getByPositionid(levelListDao.getPositionid());
            Level level = levelRepository.findByLevelid(levelListDao.getLevelid());
            tempLevelList.setLevellistid(null);
            tempLevelList.setPosition(position);
            tempLevelList.setLevel(level);
            tempLevelList.setDatecreated(now);
            return levelListRepository.saveAndFlush(tempLevelList);
        } else {
            LevelList response = new LevelList();
            response.setLevellistid(0);
            return response;
        }
    }

    public LevelList updateLevelList(LevelListDao levelListDao){
        LevelList levelListFromDB = getById(levelListDao.getLevellistid());
        LevelList checkRecord = getByRecord(levelListDao.getPositionid(), levelListDao.getLevelid());
        if (!Objects.isNull(levelListFromDB)){
            if (levelListFromDB.getPosition().getPositionid().equals(levelListDao.getPositionid()) && levelListFromDB.getLevel().getLevelid().equals(levelListDao.getLevelid())){
                Position position = positionRepository.getByPositionid(levelListDao.getPositionid());
                Level level = levelRepository.findByLevelid(levelListDao.getLevelid());
                levelListFromDB.setPosition(position);
                levelListFromDB.setLevel(level);
                return levelListRepository.saveAndFlush(levelListFromDB);
            } else {
                if (Objects.isNull(checkRecord)) {
                    Position position = positionRepository.getByPositionid(levelListDao.getPositionid());
                    Level level = levelRepository.findByLevelid(levelListDao.getLevelid());
                    levelListFromDB.setPosition(position);
                    levelListFromDB.setLevel(level);
                    return levelListRepository.saveAndFlush(levelListFromDB);
                } else {
                    LevelList response = new LevelList();
                    response.setLevellistid(0);
                    return response;
                }
            }
        } else {
            return new LevelList();
        }
    }

    public Integer deleteLevelList(Integer id){
        return levelListRepository.removeByLevellistid(id);
    }

}
