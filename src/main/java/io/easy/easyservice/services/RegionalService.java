package io.easy.easyservice.services;

import io.easy.easyservice.models.dao.RegionalDao;
import io.easy.easyservice.models.entity.Regional;
import io.easy.easyservice.models.repository.RegionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class RegionalService {

    @Autowired
    private RegionalRepository repository;

    public List<Regional> getAll(){
        return repository.findAll();
    }

    public Regional getById(Integer id) {
        return repository.getByRegionalid(id);
    }

    public Regional getByName(String regname){
        return repository.getByRegionaldept(regname);
    }

    public Regional createNew(RegionalDao regionalDao){
        Regional checkAvailability = getByName(regionalDao.getRegionaldept());
        if(Objects.isNull(checkAvailability)){
            LocalDateTime now = LocalDateTime.now();
            Regional tempReg = new Regional();
            tempReg.setRegionalid(null);
            tempReg.setRegionaldept(regionalDao.getRegionaldept());
            tempReg.setDatecreated(now);
            return repository.saveAndFlush(tempReg);
        } else {
            Regional response = new Regional();
            response.setRegionaldept("Regional Name Registered. Cannot Save Duplicate Data");
            return response;
        }
    }

    public Regional updateRegional(RegionalDao regionalDao){
        Regional regionalFromDB = getById(regionalDao.getRegionalid());
        Regional checkRegional = getByName(regionalDao.getRegionaldept());
        if (!Objects.isNull(regionalFromDB)){
            if (regionalFromDB.getRegionaldept().equals(regionalDao.getRegionaldept())){
                regionalFromDB.setRegionaldept(regionalDao.getRegionaldept());
                return repository.saveAndFlush(regionalFromDB);
            } else {
                if (Objects.isNull(checkRegional)){
                    regionalFromDB.setRegionaldept(regionalDao.getRegionaldept());
                    return repository.saveAndFlush(regionalFromDB);
                } else {
                    Regional response = new Regional();
                    response.setRegionaldept("Regional Name Registered. Cannot Save Duplicate Data");
                    return response;
                }
            }
        }else{
            return new Regional();
        }
    }

    public Integer deleteRegional(Integer id){
        return repository.removeByRegionalid(id);
    }



}
