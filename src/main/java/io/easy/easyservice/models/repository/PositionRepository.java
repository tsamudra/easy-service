package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.MainJob;
import io.easy.easyservice.models.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface PositionRepository extends JpaRepository<Position, Integer> {

    Position getByPositionid(Integer id);
    Position getByPositionname(String postname);
    Position getByMainJobAndPositionname(MainJob mainJob, String positionname);
    @Transactional
    Integer removeByPositionid(Integer id);
}
