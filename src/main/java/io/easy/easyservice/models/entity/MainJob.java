package io.easy.easyservice.models.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "mainjob")
@Data
public class MainJob {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer mainjobid;
    private String mainjobname;
    private LocalDateTime datecreated;

}
