package io.easy.easyservice.models.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportEmployeeDao {
    private Integer employeeid;
    private String callsign;
    private String nikktp;
    private String nik;
    private String fullname;
    private String companyname;
    private String regionaldept;
    private String divisioname;
    private String mainjobname;
    private String positionname;
    private String levelname;
    private String educationname;
    private String joindate;
    private String resigndate;
    private String joincallsign;
    private String releasecallsign;
    private String address;
    private String email;
    private String phonenum;
    private String remark;
    private String remark_resign;
    private String remark_promote;
    private String remark_movement;
    private String remark_inactive;
    private String status;
    private String createdby;
}
