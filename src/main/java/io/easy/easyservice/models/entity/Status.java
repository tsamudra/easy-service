package io.easy.easyservice.models.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "status")
@Data
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer statusid;
    private String status;
    private String statuscode;
    private LocalDateTime datecreated;
}
