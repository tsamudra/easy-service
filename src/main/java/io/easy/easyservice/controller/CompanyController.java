package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.CompanyDao;
import io.easy.easyservice.models.entity.Company;
import io.easy.easyservice.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping("/getcompany")
    public List<Company> getAllCompany(){
        return companyService.getAllCompany();
    }

    @GetMapping("/getcompanybyid")
    public Company getCompanybyId(@RequestParam Integer id){
        return companyService.getCompanybyID(id);
    }

    @GetMapping("/getcompanybyname")
    public Company getCompanyByName(@RequestParam String companyname) {
        return companyService.getCompanybyName(companyname);
    }

    @PostMapping("/createcompany")
    public Company createCompany(@RequestBody CompanyDao companyDao) {
        return companyService.createNewCompany(companyDao);
    }

    @PatchMapping("/updatecompany")
    public Company updateCompany(@RequestBody CompanyDao companyDao) {
        return companyService.updateCompanyDetails(companyDao);
    }

    @DeleteMapping("/deletecompany")
    public Integer deleteCompany(@RequestParam Integer id){
        return companyService.deleteCompany(id);
    }

}
