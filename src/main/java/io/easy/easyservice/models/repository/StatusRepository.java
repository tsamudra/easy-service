package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StatusRepository extends JpaRepository<Status, Integer> {
    List<Status> findAllByStatuscode(String code);
    Status findByStatus(String status);
}
