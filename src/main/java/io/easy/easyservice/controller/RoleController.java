package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.RoleDao;
import io.easy.easyservice.models.entity.Role;
import io.easy.easyservice.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/getroles")
    public List<Role> getAllRole(){
        return roleService.getAllRoles();
    }

    @GetMapping("/getrolebyid")
    public Role getRolebyId(@RequestParam Integer id) {
        return roleService.getRoleDetailsbyId(id);
    }

    @GetMapping("/getrolebyname")
    public Role getRolebyName(@RequestParam String rolename){
        return roleService.getRoleDetailsbyRolename(rolename);
    }

    @PostMapping("/createrole")
    public Role createNewRole(@RequestBody RoleDao roleDao) {
        return roleService.createNewRole(roleDao);
    }

    @PatchMapping("/updaterole")
    public Role updateRole(@RequestBody RoleDao roleDao) {
        return roleService.updateRole(roleDao);
    }

    @DeleteMapping("/deleterole")
    public Integer deleteRole(@RequestParam Integer id) {
        return roleService.deleteRole(id);
    }

}
