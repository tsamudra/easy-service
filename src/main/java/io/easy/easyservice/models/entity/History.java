package io.easy.easyservice.models.entity;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "history")
@Data
public class History {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer historyid;
    private String historycode;
    private String type;
    private String historyfromto;
    private Integer employeeidfrom;
    private Integer employeeidto;
    private String callsignfrom;
    private String callsignto;
    private String levelfrom;
    private String levelto;
    private String divisionfrom;
    private String divisionto;
    private String companyfrom;
    private String companyto;
    private String regionalfrom;
    private String regionalto;
    private String mainjobfrom;
    private String mainjobto;
    private String positionfrom;
    private String positionto;
    private String joincallsign;
    private LocalDateTime createddate;
    private String createdby;

}
