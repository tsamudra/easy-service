package io.easy.easyservice.models.repository;

import io.easy.easyservice.models.entity.Regional;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface RegionalRepository extends JpaRepository<Regional, Integer> {

    Regional getByRegionalid(Integer id);
    Regional getByRegionaldept(String regname);
    @Transactional
    Integer removeByRegionalid(Integer id);
}
