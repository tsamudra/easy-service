package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.RegionalListDao;
import io.easy.easyservice.models.entity.RegionalList;
import io.easy.easyservice.services.RegionalListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
public class RegionalListcontroller {
    @Autowired
    private RegionalListService regionalListService;

    @GetMapping("getallregionallist")
    public List<RegionalList> getAll(){
        return regionalListService.getAll();
    }

    @GetMapping("/getregionallistbyid")
    public RegionalList getById(@RequestParam Integer id){
        return regionalListService.getById(id);
    }

    @GetMapping("/getregionallistbyrecord")
    public RegionalList getByRecord(@RequestParam Integer regionalid, @RequestParam Integer divisionid){
        return regionalListService.getByRecord(regionalid, divisionid);
    }

    @PostMapping("/createregionallist")
    public RegionalList createRegionalList(@RequestBody RegionalListDao regionalListDao){
        return regionalListService.createRegionalList(regionalListDao);
    }

    @PatchMapping("/updateregionallist")
    public RegionalList updateRegionalList(@RequestBody RegionalListDao regionalListDao){
        return regionalListService.updateRegionalList(regionalListDao);
    }

    @DeleteMapping("/deleteregionallist")
    public Integer deleteRegionalList(@RequestParam Integer id){
        return regionalListService.deleteRegionalList(id);
    }
}
