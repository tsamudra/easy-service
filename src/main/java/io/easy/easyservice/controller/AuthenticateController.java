package io.easy.easyservice.controller;

import io.easy.easyservice.models.dao.AuthenticateResponse;
import io.easy.easyservice.models.dao.AuthenticationRequest;
import io.easy.easyservice.models.entity.User;
import io.easy.easyservice.services.MyUserDetailsService;
import io.easy.easyservice.services.UserService;
import io.easy.easyservice.utilities.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthenticateController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @PostMapping("/login")
    public AuthenticateResponse createAuthenticateToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try {
            System.out.println(authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            ));
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );

        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        User userDetail = userService.getUserDetailsbyUsername(authenticationRequest.getUsername());

        final String jwt = jwtTokenUtil.generateToken(userDetails);

        AuthenticateResponse response = new AuthenticateResponse(jwt, userDetail.getUserid(), userDetail.getRole().getRoleid(), userDetail.getCompany().getCompanyid(), userDetail.getRegional().getRegionalid(), userDetail.getUsername(), userDetail.getFullname());

        return response;
    }

}
